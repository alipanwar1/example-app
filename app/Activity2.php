<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Thomasjohnkane\Snooze\Traits\SnoozeNotifiable;
use Illuminate\Notifications\Notifiable;

class Activity2 extends Model
{
    use Notifiable, SnoozeNotifiable;

    protected $table = "activity2";
    protected $primaryKey = "id";
    protected $fillable =
    [
        'id',
        'name',
        'counterpart',
        'pic_counterpart',
        'typeactivity',
        'activityperiod',
        'date_start',
        'date_finish',
        'due_date',
        'day_reminder',
        'media',
        'media_content',
        'status',
        'regulasi',
        'remarks',
        'email',
    ];

    //public function routeNotificationForMail(Notification $notification): string
   // {
        // Return email address only...
    //    return 'anwar.raharja@bahana.co.id';
 
        // Return email address and name...
        //return ['anwar.raharja@bahana.co.id' => 'Anwar Raharja'];
    //}    

}
