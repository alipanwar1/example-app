<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParameterAmlPtb extends Model
{
    protected $connection = 'amlptb';
    protected $table = "t_parameter";
    protected $fillable = ['val'];
    const UPDATED_AT = null;
    
}
