<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Group;
use App\Sales;
use App\Dealer;
use App\Counterpart;

use DataTables;

class CounterpartController extends Controller
{
    public function sales()
    {        
        $counterpart_list = Counterpart::all();
        $row = 0;
        foreach ($counterpart_list as $p){
            $row = $p->count;
        }

        $role_app = Auth::user()->role_app;
        $clapp = DB::select('SELECT cl_permission_app.clp_role_app, cl_app.* FROM cl_app 
                                    JOIN cl_permission_app ON cl_permission_app.clp_app = cl_app.cla_id
                                    JOIN role_app ON cl_permission_app.clp_role_app = role_app.id
                                    WHERE cl_app.cla_shown = 1 
                                    AND cl_permission_app.clp_role_app = '.$role_app.'
                                    ORDER BY cl_app.cla_order;
                            ');

        $permission = DB::select('SELECT count(*) count FROM cl_permission_app_mod 
                            JOIN cl_app_mod ON cl_permission_app_mod.clp_app_mod = cl_app_mod.id
                            JOIN cl_module ON cl_module.clm_id = cl_app_mod.clam_clm_id
                            WHERE cl_module.clm_slug = \'compliance\' AND cl_permission_app_mod.clp_role_app = '.$role_app);

        $countpermission = 0;
        foreach ($permission as $p){
            $countpermission = $p->count;
        }        
        if ($countpermission === 0 || $countpermission === '0'){
            return view('permission');
        } else {
            $clapps = DB::select('SELECT cl_app.* FROM cl_app WHERE cl_app.cla_routename = \'compliance\' ');
            $clmodule = DB::select('SELECT cl_module.* FROM cl_module WHERE cl_module.clm_slug = \'counterpart\' ');
            return view('user-admin/counterpart',
                [
                    'title' => 'Counterpart',
                    'group' => $counterpart_list,
//                'newid'=>$this->get_prime(),
                    'countgroup' => $row,
                    'clapp' => $clapp,
                    'role_app' => $role_app,
                    'clapps' => $clapps,
                    'clmodule' => $clmodule
                ]
            );
        }
    }

    public function salesEdit()
    {
        $counterpart_id = $_GET['counterpart_id'];
        $sales = Counterpart::where('counterpart_id',$counterpart_id)->get()[0];
        return response()->json($sales);
    }


    public function updateSales(){
        $counterpart_id = $_GET['counterpart_id'];
        $name = $_GET['name'];
        $phone = $_GET['phone'];
        $email = $_GET['email'];
        $status = $_GET['status'];
        try {
            $query = Counterpart::where('counterpart_id', $counterpart_id)->update([
                'name' => $name,
                'address' => $address,
                'phone' => $phone,
                'email' => $email,
                'status' => $status
            ]);
            $status = "00";
            $group = $name;
            $err_msg = "Success";
            saveActivityLog('Update',"Update Counterpart - $group - $err_msg");

        }catch(QueryException $ex){
            $status = "01";
            $group = null;
            $err_msg = $ex->getMessage();
            saveActivityLog('Update',"Update Counterpart - $group - $err_msg");

        }

        return response()->json([
            'status' => $status,
            'group' => $group,
            'err_msg' => $err_msg,
            
        ]);
    }

    public function store(Request $request){
        echo "ok";
    }

    public function get_prime(){
        $count = 0;
        if(Group::max("group_id") < 1){
            $num = 0;
        }else{
            $num = Group::max("group_id") + 1;
        }
        if($num == 1){
            $num = 2;
        }
        while ($count < 1 )
        {
            $div_count=0;
            for ( $i=1; $i<=$num; $i++)
            {
                if (($num%$i)==0)
                {
                    $div_count++;
                }
            }
            if ($div_count<3)
            {
                return $num;
                $count=$count+1;
            }
            $num=$num+1;
        }
    }

    public function getIdGroup(){
        $groupID = (int)$this->get_prime();
        return response()->json([
            'groupID' => $groupID,
        ]);
    }

    function registrasiSales(){
        $counterpart_id = $_GET['counterpart_id'];
        $name = $_GET['name'];
        $address = $_GET['address'];
        $phone = $_GET['phone'];
        $email = $_GET['email'];
        $status = $_GET['status'];
        try{
            $query = Sales::create([
                'counterpart_id' => $counterpart_id,
                'name' => $name,
                'address' => $address,
                'phone' => $phone,
                'email' => $email,
                'status'=> $status,
            ]);
            $status = "00";
            $group = $name;
            $err_msg = "Success";

            saveActivityLog('Create',"Create Counterpart - $group - $err_msg");
            
        }catch(QueryException $ex){
            $status = '01';
            $group = null;
            $err_msg = $ex->getMessage();

            saveActivityLog('Create',"Create Counterpart - $group - $err_msg");

        }

        return response()->json([
            'status' => $status,
            'group' => $group,
            'err_msg' => $err_msg,
        ]);
    }

    public function getCounterpart(Request $request){
        
        $requestData = $request->all();
        $where_groupID = "";
        if($request->has('search_param')){

            $counterpartID = $requestData['search_param']['counterpart_id'];
            if ($counterpartID != ""){
                $where_groupID = ' WHERE counterpart_id ='.$counterpartID;
            }
        }
        //$query = 'SELECT *,sales.sales_id as sls from "sales"
        $query = 'SELECT *,counterpart.counterpart_id as sls from counterpart
                  '.$where_groupID;
                  Log::info($query);
        $data = DB::select($query);
        return DataTables::of($data)->make(true);
    }

    public function getSalesName(){
        $counterpart_id = $_GET['counterpartID'];
Log::info("getSalesName");
        Log::info($counterpart_id);
        $group = Counterpart::select('counterpart_id')
            ->where('counterpart_id',$counterpart_id)
            ->get();
        return response()->json($group);
    }

    public function getIdSales(){
        $counterpart_id = $_GET['counterpart_id'];
        $res = Counterpart::where('counterpart_id',$counterpart_id)->count();
        if($res > 0){
            $status = "01";
        }else{
            $status = "00";
        }
        return response()->json([
            'status' => $status,
        ]);
    }

    public function getGroupUser(Request $request){
        $requestData = $request->all();        
        $groupID = $requestData['search_param']['groupID'];

        if ($groupID === '' || $groupID === null){
            $groupID = 0;
        }

        $query = 'SELECT 
                    ROW_NUMBER() OVER (ORDER BY group_id)  sequence_no,
                    "view_user_group_dealer".* 
                  FROM "view_user_group_dealer" 
                  WHERE "group_id" ='.$groupID;
        $data = DB::select($query);
        return DataTables::of($data)->make(true);
    }

    public function dataGroup(Request $request){
        $requestData = $request->all();

        $groupID = $requestData['search_param']['groupID'];
        $where_groupID = "";
        if ($groupID != ""){
            $where_groupID = ' WHERE "group".group_id = '.$groupID;
        }

        $query = 'SELECT * from "group"
                  '.$where_groupID;
        $data = DB::select($query);
        return DataTables::of($data)->make(true);
    }

}
