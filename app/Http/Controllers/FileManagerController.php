<?php

namespace App\Http\Controllers;

use Session;
use App\RoleApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\MyFileSystem;
use App\UploadFiles;

class FileManagerController extends Controller
{
    //
    public function dashboard()
    {
        $roleApp = RoleApp::orderBy('id','ASC')->get();

        $role_app = Auth::user()->role_app;
        $clapp = DB::select('SELECT cl_permission_app.clp_role_app, cl_app.* FROM cl_app 
                                    JOIN cl_permission_app ON cl_permission_app.clp_app = cl_app.cla_id
                                    JOIN role_app ON cl_permission_app.clp_role_app = role_app.id
                                    WHERE cl_app.cla_shown = 1 
                                    AND cl_permission_app.clp_role_app = '.$role_app.'
                                    ORDER BY cl_app.cla_order;
                            ');

        $permission = DB::select('SELECT count(*) count FROM cl_permission_app
                            JOIN cl_app ON cl_permission_app.clp_app = cl_app.cla_id
                            WHERE cl_app.cla_routename = \'filemanager\' AND cl_permission_app.clp_role_app = '.$role_app);

        $countpermission = 0;
        foreach ($permission as $p){
            $countpermission = $p->count;
        }

        if ($countpermission === 0  || $countpermission === '0'){
            return view('permission');
        } else {
            $idlogin = Auth::user()->id;
            $clapps = DB::select('SELECT cl_app.* FROM cl_app WHERE cl_app.cla_routename = \'filemanager\' ');
            $clmodule = DB::select('SELECT cl_module.* FROM cl_module WHERE cl_module.clm_slug = \'filemanager\' ');
            
            return view('filemanager', compact('clapp', 'role_app', 'roleApp','idlogin','clapps','clmodule'), ['title' => 'File Manager']);
        }
    }

    public function dashboardfull()
    {
        $roleApp = RoleApp::orderBy('id','ASC')->get();

        $role_app = Auth::user()->role_app;
        $clapp = DB::select('SELECT cl_permission_app.clp_role_app, cl_app.* FROM cl_app 
                                    JOIN cl_permission_app ON cl_permission_app.clp_app = cl_app.cla_id
                                    JOIN role_app ON cl_permission_app.clp_role_app = role_app.id
                                    WHERE cl_app.cla_shown = 1 
                                    AND cl_permission_app.clp_role_app = '.$role_app.'
                                    ORDER BY cl_app.cla_order;
                            ');

        $permission = DB::select('SELECT count(*) count FROM cl_permission_app
                            JOIN cl_app ON cl_permission_app.clp_app = cl_app.cla_id
                            WHERE cl_app.cla_routename = \'dashboardfull\' AND cl_permission_app.clp_role_app = '.$role_app);

        $countpermission = 0;
        foreach ($permission as $p){
            $countpermission = $p->count;
        }

        if ($countpermission === 0  || $countpermission === '0'){
            return view('permission');
        } else {
            $idlogin = Auth::user()->id;
            $clapps = DB::select('SELECT cl_app.* FROM cl_app WHERE cl_app.cla_routename = \'dashboardfull\' ');
            $clmodule = DB::select('SELECT cl_module.* FROM cl_module WHERE cl_module.clm_slug = \'dashboardfull\' ');
            $fullscreendashboard=1;
            return view('dashboards-full', compact('clapp', 'role_app', 'roleApp','idlogin','clapps','clmodule','fullscreendashboard'), ['title' => 'Dashboard']);
        }
    }

    public function dashboardaml(Request $request){
        $roleApp = RoleApp::orderBy('id','ASC')->get();

        $role_app = Auth::user()->role_app;
        $clapp = DB::select('SELECT cl_permission_app.clp_role_app, cl_app.* FROM cl_app 
                                    JOIN cl_permission_app ON cl_permission_app.clp_app = cl_app.cla_id
                                    JOIN role_app ON cl_permission_app.clp_role_app = role_app.id
                                    WHERE cl_app.cla_shown = 1 
                                    AND cl_permission_app.clp_role_app = '.$role_app.'
                                    ORDER BY cl_app.cla_order;
                            ');

        $permission = DB::select('SELECT count(*) count FROM cl_permission_app
                            JOIN cl_app ON cl_permission_app.clp_app = cl_app.cla_id
                            WHERE cl_app.cla_routename = \'dashboardaml\' AND cl_permission_app.clp_role_app = '.$role_app);

        $countpermission = 0;
        foreach ($permission as $p){
            $countpermission = $p->count;
        }

        if ($countpermission === 0  || $countpermission === '0'){
            return view('permission');
        } else {
            $idlogin = Auth::user()->id;
            $clapps = DB::select('SELECT cl_app.* FROM cl_app WHERE cl_app.cla_routename = \'dashboardaml\' ');
            $clmodule = DB::select('SELECT cl_module.* FROM cl_module WHERE cl_module.clm_slug = \'dashboardaml\' ');
            
            $summary = [
                'mtc'   => 0,
                'ftr'   => 0,
                'whs'   => 0,
                'unt'   => 0,
                'fds'   => 0,
                'aop'   => 0,
                'uct'   => 0,
                'unc'   => 0
            ];
            $q_aml = DB::connection('amlptb')->table('t_suspect_aml');
            $q_ptb = DB::connection('amlptb')->table('t_suspect_ptb');
            $filter = $request->get('date', date('Y-m-d'));
            $date = explode('|', $filter);
            if (count($date) > 1) {
                $start_date = $date[0];
                $end_date = $date[1];
                $q_aml->whereDate('process_code', '>=', $date[0])->whereDate('process_code', '<=', $date[1]);
                $q_ptb->whereDate('process_code', '>=', $date[0])->whereDate('process_code', '<=', $date[1]);
            } else {
                $start_date = $date[0];
                $end_date = $date[0];
                $q_aml->whereDate('process_code', $date[0]);
                $q_ptb->whereDate('process_code', $date[0]);
            }
            $aml = $q_aml->get();
            foreach ($aml as $v) {
                $summary['aop'] = $summary['aop'] + $v->asset_out_of_profile_1_1 + $v->asset_out_of_profile_1_2 + $v->asset_out_of_profile_1_3;
                $summary['uct'] = $summary['uct'] + $v->unusual_cash_transaction_2_1;
                $summary['unc'] = $summary['unc'] + $v->unusual_non_cash_transaction_3_1 + $v->unusual_non_cash_transaction_3_2 + $v->unusual_non_cash_transaction_3_3 + $v->unusual_non_cash_transaction_3_4 + $v->unusual_non_cash_transaction_3_5 + $v->unusual_non_cash_transaction_3_6;
            }
            $ptb = $q_ptb->select([
                DB::raw('COALESCE(SUM(marking_the_close),0) AS mtc'),
                DB::raw('COALESCE(SUM(front_running),0) AS ftr'),
                DB::raw('COALESCE(SUM(wash_sale),0) AS whs'),
                DB::raw('COALESCE(SUM(unusual_transaction),0) AS unt'),
                DB::raw('COALESCE(SUM(fake_demand_supply),0) AS fds')
            ])->first();
            if ($ptb) {
                $summary['mtc'] = $ptb->mtc;
                $summary['ftr'] = $ptb->ftr;
                $summary['whs'] = $ptb->whs;
                $summary['unt'] = $ptb->unt;
                $summary['fds'] = $ptb->fds;
            }

            return view('dashboards-aml', compact('clapp', 'role_app', 'roleApp','idlogin','clapps','clmodule', 'summary', 'start_date', 'end_date'), ['title' => 'Dashboard']);
          }

    }
    public function countUserActivityLogin(){
        //date('Y-m-d')
        // $query = DB::connection('pgsql2')->select('SELECT DISTINCT
        //             (SELECT COUNT(DISTINCT user_id) cnt_web FROM user_activity WHERE terminal=\'web\' AND activity=\'LOGIN\' 
        //             AND status=\'SUCCESS\' AND timestamp::date=\'2020-10-12\'),
        //             (SELECT COUNT(DISTINCT user_id) cnt_mobile FROM user_activity WHERE terminal=\'mobile\' AND activity=\'LOGIN\' 
        //             AND status=\'SUCCESS\' AND timestamp::date=\'2020-10-12\'),
        //             (SELECT COUNT(DISTINCT user_id) cnt_pc FROM user_activity WHERE terminal=\'pc\' AND activity=\'LOGIN\'
        //             AND status=\'SUCCESS\' AND timestamp::date=\'2020-10-12\'),
        //             (SELECT COUNT(DISTINCT user_id) cnt_web_mobile FROM user_activity WHERE terminal IN (\'web\',\'mobile\')
        //             AND activity=\'LOGIN\' AND status=\'SUCCESS\' AND timestamp::date=\'2020-10-12\'),
        //             CONCAT (CAST(to_char(now(), \'dd Mon YYYY HH24:MI:ss\') as varchar(4000)),\' WIB\') as now_date,
        //             now() as dt
        //             FROM user_activity');
        $now = date('Y-m-d');
        $query1 = "SELECT DISTINCT
                    (SELECT COUNT(DISTINCT user_id) cnt_web FROM user_activity WHERE terminal='web' AND activity='LOGIN' 
                    AND status in ('OK','SUCCESS') AND timestamp::date='$now'),
                    (SELECT COUNT(DISTINCT user_id) cnt_mobile FROM user_activity WHERE terminal='mobile' AND activity='LOGIN' 
                    AND status in ('OK','SUCCESS') AND timestamp::date='$now'),
                    (SELECT COUNT(DISTINCT user_id) cnt_pc FROM user_activity WHERE terminal='pc' AND activity='LOGIN'
                    AND status in ('OK','SUCCESS') AND timestamp::date='$now'),
                    (SELECT COUNT(DISTINCT user_id) cnt_web_mobile FROM user_activity WHERE terminal IN ('web','mobile')
                    AND activity='LOGIN' AND status in ('OK','SUCCESS') AND timestamp::date='$now'),
                    CONCAT (CAST(to_char(now(), 'dd Mon YYYY HH24:MI:ss') as varchar(4000)),' WIB') as now_date,
                    now() as dt FROM user_activity";
        $data1 = DB::select($query1);

        $query2 = "SELECT DISTINCT (Select sum(trade_value) sum_web_trade from v_trades WHERE source = 'Web'),
        (Select sum(trade_value) sum_mobile_trade from v_trades WHERE source = 'Mobile'),
        (Select sum(trade_value) sum_dealer_trade from v_trades WHERE source = 'Dealer'),
        (Select sum(trade_value) sum_trade_all from v_trades) FROM v_trades";
        $data2 = DB::select($query2);

// $query2 = DB::connection('pgsql2')->select('SELECT DISTINCT (Select sum(trade_value) sum_web_trade from v_trades WHERE source = \'Web\'),
        //             (Select sum(trade_value) sum_mobile_trade from v_trades WHERE source = \'Mobile\'),
        //             (Select sum(trade_value) sum_dealer_trade from v_trades WHERE source = \'Dealer\'),
        //             (Select sum(trade_value) sum_trade_all from v_trades) FROM v_trades');

        $query3 = "SELECT DISTINCT (SELECT SUM(order_value) sum_web_order FROM v_order_all WHERE source = 'Web'),
        (SELECT SUM(order_value) sum_mobile_order FROM v_order_all WHERE source = 'Mobile'),
        (SELECT SUM(order_value) sum_dealer_order FROM v_order_all WHERE source = 'Dealer'),
        (SELECT SUM(order_value) sum_order_all FROM v_order_all) FROM v_order_all";
        $data3 = DB::select($query3);

        // $query3 = DB::connection('pgsql2')->select('SELECT DISTINCT (SELECT SUM(order_value) sum_web_order FROM v_order_all WHERE source = \'Web\'),
        //             (SELECT SUM(order_value) sum_mobile_order FROM v_order_all WHERE source = \'Mobile\'),
        //             (SELECT SUM(order_value) sum_dealer_order FROM v_order_all WHERE source = \'Dealer\'),
        //             (SELECT SUM(order_value) sum_order_all FROM v_order_all) FROM v_order_all');

        $query4 = "SELECT DISTINCT (SELECT COUNT(*) cnt_web_order FROM v_order_all WHERE source = 'Web'),
        (SELECT COUNT(*) cnt_mobile_order FROM v_order_all WHERE source = 'Mobile'),
        (SELECT COUNT(*) cnt_dealer_order FROM v_order_all WHERE source = 'Dealer') FROM v_order_all";
        $data4 = DB::select($query4);

        // $query4 = DB::connection('pgsql2')->select('SELECT DISTINCT (SELECT COUNT(*) cnt_web_order FROM v_order_all WHERE source = \'Web\'),
        //             (SELECT COUNT(*) cnt_mobile_order FROM v_order_all WHERE source = \'Mobile\'),
        //             (SELECT COUNT(*) cnt_dealer_order FROM v_order_all WHERE source = \'Dealer\') FROM v_order_all');

        $query5 = "SELECT COUNT(DISTINCT base_account_no) cnt_trades FROM trades";
        $data5 = DB::select($query5);

        // $query5 = DB::connection('pgsql2')->select('SELECT COUNT(DISTINCT base_account_no) cnt_trades FROM trades');

        $query6 = "SELECT COUNT(DISTINCT base_account_no) cnt_orders FROM v_order_all";
        $data6 = DB::select($query6);

        // $query6 = DB::connection('pgsql2')->select('SELECT COUNT(DISTINCT base_account_no) cnt_orders FROM v_order_all');

        return response()->json(array_merge(['user_activity' => $data1],['sum_trade' => $data2],
            ['sum_order' => $data3],['number_order' => $data4],
            ['trades' => $data5],['orders' => $data6]));
    }

    public function datatopTrade(Request $request){
        $requestData = $request->all();

        $tableType = $requestData['search_param']['tableType'];

        if ($tableType === '1'){
            // $data = DB::connection('pgsql2')->select('SELECT a.sales_id AS user_code, b.sales_name AS user_name, 
            //                                                   SUM(trade_value) AS total_val FROM v_trade_sales a,sales b 
            //                                                   WHERE a.sales_id=b.sales_id GROUP BY a.sales_id, sales_name ORDER BY total_val DESC LIMIT 10;');

            $query = "SELECT a.sales_id AS user_code, b.sales_name AS user_name, SUM(trade_value) AS total_val FROM v_trade_sales a,sales b WHERE a.sales_id=b.sales_id GROUP BY a.sales_id, sales_name ORDER BY total_val DESC LIMIT 10";  
    } else {
            $query = "SELECT base_account_no AS user_code, b.custname AS user_name, SUM(trade_value) AS total_val FROM v_trades a, customer b WHERE a.base_account_no=b.custcode GROUP BY base_account_no, custname ORDER BY total_val DESC LIMIT 10";
            
            // $data = DB::connection('pgsql2')->select('SELECT base_account_no AS user_code, b.custname AS user_name, 
            //                                                   SUM(trade_value) AS total_val FROM v_trades a, customer b 
            //                                                   WHERE a.base_account_no=b.custcode GROUP BY base_account_no, custname ORDER BY total_val DESC LIMIT 10;');
        }
        $data = DB::select($query);        
        return DataTables::of($data)->make(true);
    }
    public function index(){
        //return view('index');
        $roleApp = RoleApp::orderBy('id','ASC')->get();

        $role_app = Auth::user()->role_app;
        $clapp = DB::select('SELECT cl_permission_app.clp_role_app, cl_app.* FROM cl_app 
                                    JOIN cl_permission_app ON cl_permission_app.clp_app = cl_app.cla_id
                                    JOIN role_app ON cl_permission_app.clp_role_app = role_app.id
                                    WHERE cl_app.cla_shown = 1 
                                    AND cl_permission_app.clp_role_app = '.$role_app.'
                                    ORDER BY cl_app.cla_order;
                            ');

        $permission = DB::select('SELECT count(*) count FROM cl_permission_app
                            JOIN cl_app ON cl_permission_app.clp_app = cl_app.cla_id
                            WHERE cl_app.cla_routename = \'filemanager\' AND cl_permission_app.clp_role_app = '.$role_app);

        $countpermission = 0;
        foreach ($permission as $p){
            $countpermission = $p->count;
        }

        if ($countpermission === 0  || $countpermission === '0'){
            return view('permission');
        } else {
            $idlogin = Auth::user()->id;
            $clapps = DB::select('SELECT cl_app.* FROM cl_app WHERE cl_app.cla_routename = \'filemanager\' ');
            $clmodule = DB::select('SELECT cl_module.* FROM cl_module WHERE cl_module.clm_slug = \'filemanager\' ');

            /*
            $directories = array();
            if ($handle = opendir(storage_path($directory))) {

                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {                    
                        $isdir = is_dir($entry);
                        if(file_exists($entry)){
                            $filesize = filesize($entry);                        
                        }
                        else{
                            $filesize = 0;
                        }                    
                        $path_parts = pathinfo($entry);
                        $folder = $path_parts['dirname'];
                        $filename = $path_parts['filename'];
                        $sls = $filename;
                        //Log::info($entry."<br>"); // NAME OF THE FILE
                        array_push($directories, new MyFileSystem($folder, $filename, $sls));
                    }
                }
                closedir($handle);
            }
            */
            //dd($directories);
            $directory = 'app'.DIRECTORY_SEPARATOR.'public';
            $files = File::allFiles(storage_path($directory));
            //dd($files);
            $directories = array();
            $pathinfos = array();
            foreach ($files as $p){
                //$folder = $p->getRelativePathname();
                $folder = $p->getRelativePath();
                $filename = $p->getFilename();
                if($folder=="")
                    $sls = $filename;
                else
                    $sls = $folder.DIRECTORY_SEPARATOR.$filename;
                
                array_push($directories, new MyFileSystem($folder, $filename, $sls));
                $pathinfo = pathinfo($p);
                array_push($pathinfos, $pathinfo);
            }

            return view('filemanager', compact('clapp', 'role_app', 'roleApp','idlogin','clapps','clmodule', 'files', 'directories'), ['title' => 'File Manager']);
        }
     }
  
     public function uploadFile(Request $request){
  
        // Validation
        //$request->validate([
        //  'file' => 'required|mimes:png,jpg,jpeg,csv,txt,pdf,zip,log|max:20480'
        //]); 
  
        $input = $request->all();
        //dd($input);
        if($request->file('file')) {
           $file = $request->file('file');
           //date_default_timezone_set('asia/jakarta');
           $date = date("YmdHis");
           //$filename = time().'_'.$file->getClientOriginalName();
           $filename = $date.'_'.$file->getClientOriginalName();
  
           $activity_id = $request->get('modalgroupid');

           Log::info($filename." ".$activity_id);
           // File upload location
           $location = 'files';
  
           // Upload file
           $file->move($location,$filename);
  
           $src = public_path('files'.DIRECTORY_SEPARATOR.$filename);
           $dest = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files2'.DIRECTORY_SEPARATOR.$filename);
           Log::info("src: ".$src);
           Log::info("dest: ".$dest);

           File::copy($src, $dest);           

           $newfolder = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'newfolder');
           if(!File::exists($newfolder)){
                File::makeDirectory($newfolder);
           }
           
           Session::flash('message','Upload Successfully.');
           Session::flash('alert-class', 'alert-success');
        }else{
           Session::flash('message','File not uploaded.');
           Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->back();        
     }
     
     public function uploadFileNew(Request $request){
  
        // Validation
        //$request->validate([
        //  'file' => 'required|mimes:png,jpg,jpeg,csv,txt,pdf,zip,log|max:20480'
        //]); 
  
        if($request->file('file')) {
           $file = $request->file('file');
           $date = date("YmdHis");
           //$filename = time().'_'.$file->getClientOriginalName();
           $filename = $date.'_'.$file->getClientOriginalName();

           $activity_id = $request->modalgroupid;

           Log::info($filename." ".$activity_id);
           // File upload location
           $location = 'files';
  
           // Upload file
           $file->move($location,$filename);
  
           $src = public_path('files'.DIRECTORY_SEPARATOR.$filename);
           $path = 'app'.DIRECTORY_SEPARATOR.'public';
           $dest = storage_path($path.DIRECTORY_SEPARATOR.$filename);
           Log::info("src: ".$src);
           Log::info("dest: ".$dest);
           File::copy($src, $dest);           
Log::info("$activity_id=".$activity_id);
Log::info("$filename=".$filename);
Log::info("$path=".$path);
           UploadFiles::create([
			'file' => $filename,
			'path' => $path,
            'activity_id' => $activity_id,
		    ]);

           /*$newfolder = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'newfolder');
           if(!File::exists($newfolder)){
                File::makeDirectory($newfolder);
           }*/
           
           Session::flash('message','Upload Successfully.');
           Session::flash('alert-class', 'alert-success');
        }else{
           Session::flash('message','File not uploaded.');
           Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->back();        
     }

     public function getFolderName(){
        $activity_id = $_GET['activity_id'];
Log::info("getSalesName");
        Log::info($activity_id);
        $group = Activity::select('activity_id')
            ->where('activity_id',$activity_id)
            ->get();
        return response()->json($group);
    }     
  
    public function getDataFolder(Request $request){
        
        Log::info("enter GetDataFolder");
        $requestData = $request->all();
        $search_folder = "";
        if($request->has('search_param')){

            $folder = $requestData['search_param']['foldername'];
            if ($folder != ""){
                $search_folder = $folder;
            }
        }
        if($search_folder !== ""){
            $directory = 'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$search_folder;
        }else{
            $directory = 'app'.DIRECTORY_SEPARATOR.'public';
        }
        /*
        Log::info("try open ".$directory);
        $directories = array();
        if ($handle = opendir(storage_path($directory))) {

            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {                    
                    $isdir = is_dir($entry);
                    if(file_exists($entry)){
                        $filesize = filesize($entry);                        
                    }
                    else{
                        $filesize = 0;
                    }                    
                    $path_parts = pathinfo($entry);
                    $folder = $path_parts['dirname'];
                    $filename = $path_parts['filename'];
                    $sls = $filename;
                    //Log::info($entry."<br>"); // NAME OF THE FILE
                    array_push($directories, new MyFileSystem($folder, $filename, $sls));
                }
            }
            closedir($handle);
        }
        */
        $directory = 'app'.DIRECTORY_SEPARATOR.'public';
        $files = File::allFiles(storage_path($directory));
        //dd($files);
        $directories = array();
        foreach ($files as $p){
            //$folder = $p->getRelativePathname();
            $folder = $p->getRelativePath();
            $filename = $p->getFilename();
            if($folder=="")
            $sls = $filename;
        else
            $sls = $folder.DIRECTORY_SEPARATOR.$filename;

            array_push($directories, new MyFileSystem($folder, $filename, $sls));
        }

        return DataTables::of($directories)->make(true);
    }

    public function selectFolder(Request $request){
        
        Log::info("enter selectFolder");
        $requestData = $request->all();        
        $search_folder = "";
        if($request->has('search_param')){
Log::info("has search_param");
            $folder = $requestData['foldername'];
            if ($folder != ""){
                $search_folder = $folder;
            }
        }
        else{
            $folder = $requestData['foldername'];
            Log::info($folder);
        }
  /*
        if($search_folder !== ""){
            $directory = 'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$search_folder;
        }else{
            $directory = 'app'.DIRECTORY_SEPARATOR.'public';
        }
        Log::info("try open ".$directory);
        $directories = array();
        if ($handle = opendir(storage_path($directory))) {

            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {                    
                    $isdir = is_dir($entry);
                    if(file_exists($entry)){
                        $filesize = filesize($entry);                        
                    }
                    else{
                        $filesize = 0;
                    }                    
                    $path_parts = pathinfo($entry);
                    $folder = $path_parts['dirname'];
                    $filename = $path_parts['filename'];
                    $sls = $folder;
                    //Log::info($entry."<br>"); // NAME OF THE FILE
                    array_push($directories, new MyFileSystem($folder, $filename, $sls));
                }
            }
            closedir($handle);
        }
    */
        $directory = 'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files2'.DIRECTORY_SEPARATOR.'1673593002_DX.pdf';
        //$file = Storage::disk('local')->get($directory);
        $filePath = storage_path($directory);
    	$headers = ['Content-Type: application/pdf'];
    	$fileName = '1673593002_DX.pdf';
        return response()->download($filePath, $fileName, $headers);
    }    
    public function download($id){
        Log::info("enter download");

        $directory = 'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR;
        $filePath = storage_path($directory.$id);
        $headers = ['Content-Type: application/pdf'];
        $fileName = '1673593002_DX.pdf';
        //return response()->download($filePath, $fileName, $headers);
        return response()->download($filePath);
    }
    public function downloadbyid($id){
        

        $uploadfiles = UploadFiles::where('id',$id)->get()[0];
        Log::info("enter downloadbyid ".$uploadfiles->id." ".$uploadfiles->file." ".$uploadfiles->path);

        $directory = 'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR;
        $filePath = storage_path($directory.$uploadfiles->file);
        $headers = ['Content-Type: application/pdf'];
        $fileName = '1673593002_DX.pdf';
        //return response()->download($filePath, $fileName, $headers);
        return response()->download($filePath);
    }

}
