<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Group;
use App\Sales;
use App\Dealer;
use App\Activity2;
use App\Counterpart;
use App\UploadFiles;
use App\Notifications\OneWeekAfterNotice;

use DataTables;
use DateTime;

class Activity2Controller extends Controller
{
    public function sales()
    {        
        //$activity_list = Activity2::all();
        $activity_list = DB::select('SELECT *,id sls FROM activity2');        
        $row = 1;
        foreach ($activity_list as $p){
            //$row = $p->count;
        }

        $role_app = Auth::user()->role_app;
        $clapp = DB::select('SELECT cl_permission_app.clp_role_app, cl_app.* FROM cl_app 
                                    JOIN cl_permission_app ON cl_permission_app.clp_app = cl_app.cla_id
                                    JOIN role_app ON cl_permission_app.clp_role_app = role_app.id
                                    WHERE cl_app.cla_shown = 1 
                                    AND cl_permission_app.clp_role_app = '.$role_app.'
                                    ORDER BY cl_app.cla_order;
                            ');

        $permission = DB::select('SELECT count(*) count FROM cl_permission_app_mod 
                            JOIN cl_app_mod ON cl_permission_app_mod.clp_app_mod = cl_app_mod.id
                            JOIN cl_module ON cl_module.clm_id = cl_app_mod.clam_clm_id
                            WHERE cl_module.clm_slug = \'compliance\' AND cl_permission_app_mod.clp_role_app = '.$role_app);

        $countpermission = 0;
        foreach ($permission as $p){
            $countpermission = $p->count;
        }        
        if ($countpermission === 0 || $countpermission === '0'){
            return view('permission');
        } else {
            $clapps = DB::select('SELECT cl_app.* FROM cl_app WHERE cl_app.cla_routename = \'compliance\' ');
            $clmodule = DB::select('SELECT cl_module.* FROM cl_module WHERE cl_module.clm_slug = \'activity\' ');
            $counterpartlist = DB::select('SELECT name from counterpart order by name');
            $typeactivity = DB::select('SELECT name from typeactivity order by name');
            $activityperiod = DB::select('SELECT name from activityperiod order by name');
            $user_admins = DB::select('SELECT username from user_admins order by username');
            $data_owner = DB::select('SELECT name from dataowner order by name');
            $media = DB::select('SELECT name from mediatype order by name');
            $status = DB::select('SELECT name from activitystatus order by name');
            
            return view('user-admin/activity2',
                [
                    'title' => 'Activity2',
                    'group' => $activity_list,
//                'newid'=>$this->get_prime(),
                    'countgroup' => $row,
                    'clapp' => $clapp,
                    'role_app' => $role_app,
                    'clapps' => $clapps,
                    'clmodule' => $clmodule,
                    'counterpartlist' => $counterpartlist,
                    'typeactivity' => $typeactivity,
                    'activityperiod' => $activityperiod,
                    'user_admins' => $user_admins,
                    'data_owner' => $data_owner,
                    'media' => $media,
                    'status' => $status,
                ]
            );
        }
    }

    public function activityEdit()
    {
        $id = $_GET['activityID'];
        $activity = Activity2::where('id',$id)->get()[0];
                
        Log::info($activity->day_reminder);
        $activity->notifyAt(new OneWeekAfterNotice, new Carbon($activity->day_reminder));

        return response()->json($activity);
    }
    public function getFiles()
    {
        Log::info("getFiles()");
        $id = $_GET['activityID'];
        $uploadFiles = UploadFiles::where('id',$id)->get();        
        return response()->json($uploadFiles);
    }

    public function updateActivity(){
        
        $id = $_GET['id'];
        $name = $_GET['name'];
        $counterpart = $_GET['counterpart'];
        $pic_counterpart = $_GET['pic_counterpart'];
        $typeactivity = $_GET['typeactivity'];
        $activityperiod = $_GET['activityperiod'];
        $date_start = $_GET['date_start'];
        $date_finish = $_GET['date_finish'];
        $due_date = $_GET['due_date'];
        
        $myDateTime = DateTime::createFromFormat('m/d/Y', $date_start);
        $date_start = $myDateTime->format('Ymd');
        $myDateTime = DateTime::createFromFormat('m/d/Y', $date_finish);
        $date_finish = $myDateTime->format('Ymd');
        $myDateTime = DateTime::createFromFormat('m/d/Y', $due_date);
        $due_date = $myDateTime->format('Ymd');
        $day_reminder = $_GET['day_reminder'];
        $myDateTime = DateTime::createFromFormat('m/d/Y', $day_reminder);
        $day_reminder = $myDateTime->format('Ymd');
        $data_owner = $_GET['data_owner'];
        $media = $_GET['media'];
        $media_content = $_GET['media_content'];
        $status = $_GET['status'];
        $regulasi = $_GET['regulasi'];
        $remarks = $_GET['remarks'];

        try {
            $query = Activity2::where('id', $id)->update([
                'name' => $name,
                'counterpart' => $counterpart,
                'pic_counterpart' => $pic_counterpart,
                'typeactivity' => $typeactivity,
                'activityperiod' => $activityperiod,
                'date_start' => $date_start,
                'date_finish' => $date_finish,
                'due_date' => $due_date,
                'day_reminder' => $day_reminder,
                'data_owner'=> $data_owner,
                'media'=> $media,
                'media_content'=> $media_content,
                'status'=> $status,
                'regulasi'=> $regulasi,
                'remarks'=> $remarks,
            ]);
            $status = "00";
            $group = $name;
            $err_msg = "Success";
            //saveActivity2Log('Update',"Update Activity2 - $group - $err_msg");

        }catch(QueryException $ex){
            $status = "01";
            $group = null;
            $err_msg = $ex->getMessage();
            //saveActivity2Log('Update',"Update Activity2 - $group - $err_msg");

        }

        return response()->json([
            'status' => $status,
            'group' => $group,
            'err_msg' => $err_msg,
            
        ]);
    }

    public function store(Request $request){
        echo "ok";
    }

    public function get_prime(){
        $count = 0;
        if(Group::max("group_id") < 1){
            $num = 0;
        }else{
            $num = Group::max("group_id") + 1;
        }
        if($num == 1){
            $num = 2;
        }
        while ($count < 1 )
        {
            $div_count=0;
            for ( $i=1; $i<=$num; $i++)
            {
                if (($num%$i)==0)
                {
                    $div_count++;
                }
            }
            if ($div_count<3)
            {
                return $num;
                $count=$count+1;
            }
            $num=$num+1;
        }
    }

    public function getIdGroup(){
        $groupID = (int)$this->get_prime();
        return response()->json([
            'groupID' => $groupID,
        ]);
    }

    function registrasiActivity(){

        Log::info("registrasiActivity2");
        
        $id = $_GET['id'];
        $name = $_GET['name'];
        $counterpart = $_GET['counterpart'];
        $pic_counterpart = $_GET['pic_counterpart'];
        $typeactivity = $_GET['typeactivity'];
        $activityperiod = $_GET['activityperiod'];
        $date_start = $_GET['date_start'];
        $myDateTime = DateTime::createFromFormat('m/d/Y', $date_start);
        $date_start = $myDateTime->format('Ymd');
        Log::info($date_start);
        $date_finish = $_GET['date_finish'];
        $myDateTime = DateTime::createFromFormat('m/d/Y', $date_finish);
        $date_finish = $myDateTime->format('Ymd');
        $due_date = $_GET['due_date'];
        $myDateTime = DateTime::createFromFormat('m/d/Y', $due_date);
        $due_date = $myDateTime->format('Ymd');
        $day_reminder = $_GET['day_reminder'];
        $myDateTime = DateTime::createFromFormat('m/d/Y', $day_reminder);
        $day_reminder = $myDateTime->format('Ymd');
        $data_owner = $_GET['data_owner'];
        $media = $_GET['media'];
        $media_content = $_GET['media_content'];
        $status = $_GET['status'];
        $regulasi = $_GET['regulasi'];
        $remarks = $_GET['remarks'];
        try{
            //DB::unprepared('SET IDENTITY_INSERT activity ON');

            DB::table('activity2')->insert([
                //'id' => $id,
                'name' => $name,
                'counterpart' => $counterpart,
                'pic_counterpart' => $pic_counterpart,
                'typeactivity' => $typeactivity,
                'activityperiod'=> $activityperiod,
                'date_start'=> $date_start,
                'date_finish'=> $date_finish,
                'due_date'=> $due_date,
                'day_reminder'=> $day_reminder,
                'data_owner'=> $data_owner,
                'media'=> $media,
                'media_content'=> $media_content,
                'status'=> $status,
                'regulasi'=> $regulasi,
                'remarks'=> $remarks,
            ]);
            //DB::unprepared('SET IDENTITY_INSERT activity OFF');
            $status = "00";
            $group = $name;
            $err_msg = "Success";
Log::info("registrasiActivity2 sukses");
            //saveActivity2Log('Create',"Create Activity2 - $group - $err_msg");
            
        }catch(QueryException $ex){
            $status = '01';
            $group = null;
            $err_msg = $ex->getMessage();

            //saveActivity2Log('Create',"Create Activity2 - $group - $err_msg");
            Log::info("registrasiActivity2 error ".$err_msg);

        }

        return response()->json([
            'status' => $status,
            'group' => $group,
            'err_msg' => $err_msg,
        ]);
    }

    public function getActivity(Request $request){
        
        Log::info("Activity2Controller enter getActivity()");
        $requestData = $request->all();
        $where_groupID = "";
        if($request->has('search_param')){

            $activityID = $requestData['search_param']['activityID'];
            if ($activityID != ""){
                $where_groupID = ' WHERE id ='.$activityID;
            }
        }
        //$query = 'SELECT *,sales.sales_id as sls from "sales"
        $query = 'SELECT *,id as sls from activity2
                  '.$where_groupID;
                  Log::info($query);
        $data = DB::select($query);

        return DataTables::of($data)->make(true);
    }

    public function getSalesName(){
        $id = $_GET['id'];
Log::info("getSalesName");
        Log::info($id);
        $group = Activity2::select('id')
            ->where('id',$id)
            ->get();
        return response()->json($group);
    }

    public function getMaxId(){

        Log::info("getMaxId");        
        $group = Activity2::max('id');        
        return response()->json($group);
    }

    public function getIdSales(){
        $id = $_GET['id'];
        $res = Activity2::where('id',$id)->count();
        if($res > 0){
            $status = "01";
        }else{
            $status = "00";
        }
        return response()->json([
            'status' => $status,
        ]);
    }

    public function getGroupUser(Request $request){
        $requestData = $request->all();        
        $groupID = $requestData['search_param']['groupID'];

        if ($groupID === '' || $groupID === null){
            $groupID = 0;
        }

        $query = 'SELECT 
                    ROW_NUMBER() OVER (ORDER BY group_id)  sequence_no,
                    "view_user_group_dealer".* 
                  FROM "view_user_group_dealer" 
                  WHERE "group_id" ='.$groupID;
        $data = DB::select($query);
        return DataTables::of($data)->make(true);
    }

    public function dataGroup(Request $request){
        $requestData = $request->all();

        $groupID = $requestData['search_param']['groupID'];
        $where_groupID = "";
        if ($groupID != ""){
            $where_groupID = ' WHERE "group".group_id = '.$groupID;
        }

        $query = 'SELECT * from "group"
                  '.$where_groupID;
        $data = DB::select($query);
        return DataTables::of($data)->make(true);
    }

}

