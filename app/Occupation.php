<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    protected $connection = 'amlptb';
    protected $table = "t_occupation";
    protected $fillable = ['occupation', 'net_asset'];
    const UPDATED_AT = null;
    const CREATED_AT = null;
}
