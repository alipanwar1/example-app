<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadFiles extends Model
{    
    protected $table = "upload_files";
 
    protected $fillable = ['file', 'path', 'activity_id'];
}
