<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public $timestamps = false;

    protected $table = "activity";
    protected $primaryKey = "activity_id";
    protected $fillable =
        [
            'activity_id',
            'name',
            'counterpart',
            'pic_counterpart',
            'typeactivity',
            'activityperiod',
            'date_start',
            'date_finish',
            'due_date',
            'day_reminder',
        ];
}
