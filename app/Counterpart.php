<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counterpart extends Model
{
    public $timestamps = false;

    protected $table = "counterpart";
    protected $primaryKey = "counterpart_id";
    protected $fillable =
        [
            'counterpart_id',
            'name',
            'email',
            'phone',
            'status',
        ];
}
