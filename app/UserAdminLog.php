<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserAdminLog extends Authenticatable
{
    public $timestamps = false;

    protected $table = "user_admins_log";
    protected $primaryKey = "id";
    protected $fillable = [
        'user_admin_id', 'action', 'description', 'time','username','role_app'
    ];
}
