<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspectAmlCustomer extends Model
{
    protected $connection = 'amlptb';
    protected $table = "t_suspect_aml_customer";
    protected $fillable = [];
}
