<?php

namespace App;

class MyFileSystem {
    
    public $folder;
    public $filename;
    public $sls;

    public function __construct($folder, $filename, $sls){

        $this->folder = $folder;
        $this->filename = $filename;
        $this->sls = $sls;
    }
}