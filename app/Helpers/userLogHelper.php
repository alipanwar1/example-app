<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

use App\UserAdminLog;

function getCustcode($id){
    $query = "SELECT customer.custcode FROM customer JOIN users ON customer.user_id = users.user_id WHERE users.user_id = '$id'";
    $data = DB::select($query);
    if($data){
        return $data[0]->custcode;
    }else{
        return '';
    }
}

function saveActivityLog($action, $desc)
{
    
    try {
        $date = new DateTime("now", new DateTimeZone('Asia/Jakarta') );
        $current_time = $date->format('Y-m-d H:i:s');
      
        $query = 'SELECT ua.username, ra.name  
                    FROM user_admins ua 
                    JOIN role_app ra ON ua.role_app = ra.id 
                    WHERE ua.id = \''.Auth::id().'\'';

        $data = DB::select($query)[0];
        
        $query = UserAdminLog::create([
            'user_admin_id' => Auth::id(),
            'action' => $action,
            'description' => $desc,
            'time' => $current_time,
            'username' => Auth::user()->username,
            'role_app' => $data->name
        ]);

        if ($query) {
            $status = '00';
            $user = Auth::user()->username;
            $message = 'Success';
        } else {
            $status = "01";
            $user = "";
            $message = 'Error';
        }
    }catch(QueryException $ex){
        $status = "01";
        $user = null;
        $message = $ex->getMessage();
    }
    return $message;
}