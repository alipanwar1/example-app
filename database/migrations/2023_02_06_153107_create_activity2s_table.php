<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivity2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity2', function (Blueprint $table) {
            $table->increments('activity_id');
            $table->string('name');
            $table->string('counterpart');
            $table->string('pic_counterpart');
            $table->string('typeactivity');
            $table->string('activityperiod');
            $table->date('date_start');
            $table->date('date_finish');
            $table->date('due_date');
            $table->date('day_reminder');
            $table->string('data_owner');
            $table->string('media');
            $table->string('media_content');
            $table->string('status');
            $table->string('regulasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity2s');
    }
}
