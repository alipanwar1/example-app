
POST https://dev-hthbg.bahana.co.id/api/registrasi-oa-online?APIKey=YmFoYW5hLXNla3VyaXRhcy1zZXJ2aWNlLWRldmVsb3BtZW50LUJITjAwMQ==
Request Headers
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2Rldi1odGhiZy5iYWhhbmEuY28uaWQvYXBpL2xvZ2luLW9hLWFwaSIsImlhdCI6MTY3MDkxOTkyNiwiZXhwIjoxNjcwOTIzNTI2LCJuYmYiOjE2NzA5MTk5MjYsImp0aSI6IjRPdjdUeGJBazg2bko4d2MiLCJzdWIiOjQxOTksInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.D949tHLNwP5_vceoE8Gp3GojfsbvW8ff_zw9S5TVdek
Content-Type: text/plain
User-Agent: PostmanRuntime/7.29.2
Accept: */*
Postman-Token: 3f0fbe02-8ea5-46bd-ac64-cf0c4ca115db
Host: dev-hthbg.bahana.co.id
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 3484
Request Body
{
    "full_name" : "testing online api",
    "email": "testdua@email.com",
    "phone_number" : "081122331123",
    "number_e_ktp" : "3674031208910010",
    "number_npwp" : "890087127891289",
    "pob" : "Bandung",
    "dob" : "1994-02-12",
    "gender" : "Male",
    "nationality" : "WNI",
    "religion_code" : "1",
    "education_code" :"105",
    "npwp_status" : "Own",
    "marital_status" : "Married",
    "spouse_name" : "Istri",
    "number_of_children" : "2",
    "home_phone" : "12345",
    "other_phone" : "08124578202",
    "mother_name" : "Ibu",
    "address_e_ktp" :{
        "address" : "Jakarta",
        "ownership_code" : "2",
        "long_stay_years" : "2",
        "long_stay_month" : "1",
        "country_code" : "1",
        "province_code" : "12",
        "city_code" : "11798",
        "districts_code" : "13822",
        "village_code" : "42088",
        "neighbourhood" : "02",
        "hamlet" : "03",
        "postal_code" : "12345"
    },
    "address_corespondence" :{
        "address" : "Jakarta",
        "ownership_code" : "2",
        "long_stay_years" : "2",
        "long_stay_month" : "1",
        "country_code" : "1",
        "province_code" : "12",
        "city_code" : "11798",
        "districts" : "kecamatan cores",
        "village" : "kelurahan cores",
        "neighbourhood" : "02",
        "hamlet" : "03",
        "postal_code" : "12345"
    },
    "address_domisili" :{
        "address" : "Jakarta",
        "ownership_code" : "2",
        "long_stay_years" : "2",
        "long_stay_month" : "1",
        "country_code" : "1",
        "province_code" : "12",
        "city_code" : "11798",
        "districts" : "kecamatan dom",
        "village" : "kelurahan dom",
        "neighbourhood" : "02",
        "hamlet" : "03",
        "postal_code" : "12345"
    },
    "occupation_data" :{
        "occupation_code" : "9",
        "industrytype_code" : "19",
        "jobposition_code" : "11",
        "source_of_income_code" : "1",
        "income_code" : "3",
        "office_name" : "PT BAHANA SEKURITAS",
        "office_addres" : "GRAHA NIAGA LT 19",
        "province_code" : "12",
        "city_code" : "247",
        "districts" : "CIBEUNYING KIDUL",
        "village" : "CIKUTRA",
        "workyear" : "06",
        "workmounth" : "02",
        "office_phone" : "628128855226",
        "nomor_ext" : "1234"
    },
    "investment_objectives" :{
        "invest_code" : "3",
        "investment_experience_code" : "1",
        "transaction_code" : "1",
        "deposittrxcount_code" : "4"
    },
    "bank_reference":{
        "bank_code": "185",
        "account_number":"0897897261",
        "account_name":"Randi",
        "bank_branch":"Lembang",
        "bank_rdn":"BMRI"
    },
    "reference_data":{
        "relative_name": "ujang kasep",
        "relative_phone_number":"0897897261",
        "beneficiary_name":"Randi",
        "relationship":"orang tua",
        "relationship_phone":"10984253453"
    },
    "reff_question":{
        "group_bahana_work":true,
        "work_other_exchanges":true,
        "shareholder_director":true,
        "presentation":"10%"
    },
   "reff_dokument":{
        "image_ktp": "",
        "image_npwp": "",
        "image_selfie": "",
        "image_signature": ""
    },
    "kyc_schedule":{
        "date_kyc":"2022-12-20",
        "time_kyc":"09:00"
    }
}