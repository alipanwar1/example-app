@extends('layouts.app-argon')

@section('css')
    <link rel="stylesheet" href="{{ url('chart/morris/morris.css') }}">
    <link rel="stylesheet" href="{{ url('bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ url('bootstrap-daterangepicker/bootstrap-daterangepicker.css') }}">
@endsection

@section('js')
    <!-- Dependencies -->
    <script>
                
        $(document).ready(function () {
            getTableFile();
            $('.js-example-basic-single').select2({
                placeholder: 'AOID'
            });
            $('.bootstrap-select').selectpicker();
        });

        function getTableFile(){
            var tableGroup = $("#table-reggroup").DataTable({
                responsive: {
                    details: true,
                },
                /*processing: true,
                serverSide: true,*/
                aaSorting: [[0, 'asc']],
                dom: 'l<"toolbar"><f "col-md-6">rtip',
                initComplete: function(){
                    $("div.toolbar").html('<button class="form-control-btn-0 btn btn-primary mb-2" id="addgroup" type="button" onclick="getMaxId()">Add</button>');
                },
                ajax : {
                    url: '{{ url("getDataFolder") }}',
                    data: function (d) {
                        var search_data = {
                            foldername: $("#foldername").val(),
                        };
                        d.search_param = search_data;
                    },
                },
                columns : [
                    {data : 'filename', name: 'filename'},
                    {data : 'folder', name: 'folder'},                    
                    {
                        data : 'sls', 
                        name: 'sls'
                    },
                ],
                columnDefs: [{
                    targets : [0],                    
                    orderable : true,
                    searchable : true,
                },{
                    targets : [1],
                    orderable : true,
                    searchable : true,
                },{
                    searchable : true,
                    targets : [2],
                    className: 'text-center',
                    render : function (data, type, row) {
                        return  '<a href="download/'+data+'" class=btn btn-success download>'+data+'</a>'
                    },
                    className: "text-center",
                },
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: 1 },
                    { responsivePriority: 3, targets: 2 },
                ],
            });
        }

        function selectFolder(data){

            console.log(data);
            $.ajax({
                type : "GET",
                        url  : "{{ url('selectfolder') }}",
                        data : {
                        'foldername' : data,
                    },
                    success : function (res) {
                        // console.log(res.name);
                        $("#breadAdditional").removeClass("d-none").addClass("d-block").text("Edit");
                        $("#breadAdditionalText").removeClass("d-none").addClass("d-block").text(res.name);
                        $("#foldername").val(data);

                }
            });

        }

        function getMaxId() {
        }

        function refreshTableList(){
            $('#table-grouplist').DataTable().ajax.reload();
        }

        function getTableList() {
            $("#table-grouplist").DataTable({
                /*processing: true,
                serverSide: true,*/
                responsive: true,
                ajax : {
                    url: '{{ url("getDataFolder") }}',
                    data: function (d) {
                        var search_data = {
                            folderName:'',
                        };
                        d.search_param = search_data;
                    },
                },
                columns : [                    
                    {data : 'filename', name: 'filename'},
                    {data : 'folder', name: 'folder'},                    
                    {data : 'sls', name: 'sls'},
                ],
                columnDefs: [{
                    targets: [0],
                    orderable: true,
                    searchable: true,
                },{
                    targets: [1],
                    orderable: true,
                    searchable: true,
                },{
                    searchable : true,
                    targets : [2],
                    className: "text-center",
                    render : function (data, type, row) {
                        return '<i class="text-center">' +
                            '<button class="btn btn-sm btn-primary" type="button" data-dismiss= "modal" onclick="clickOK(\''+data+'\')">Pick</button></i>'}
                },
                ]
            });
        }
        function clickOK(foldername) {
            $("#foldername").val(foldername);
            // alert($("#salesID").val());
            getGroup();
        }

        function getFolderName() {
            var foldername = $("#foldername").val();
            console.log(activity_id);
            if(activity_id === ''){
                $("#groupGet").val('');
                $('#table-reggroup').DataTable().ajax.reload();
            } else {
                $.ajax({
                    type : "GET",
                    url  : "{{ url('folderGetName') }}",
                    data : {
                        'foldername' : foldername,
                    },
                    success : function (res) {
                        if ($.trim(res)){
                            $("#groupGet").val(res[0].filename);
                        } else {
                            $("#groupGet").val('');
                        }
                        $('#table-reggroup').DataTable().ajax.reload();
                    }
                });
            }
        }
    </script>
@endsection

@section('content')
    <div class="modal-ajax"></div>
    <div class="header text-white">
        <div class="row col-xs-0">
            <div class="col-sm-12 col-xs-12">
                <nav aria-label="breadcrumb" class="d-inline-block ml-0 w-100">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark mb-2">
                        {{--<li class="breadcrumb-item"><a href="#"><i class="ni ni-single-02"></i> Dashboards</a></li>--}}
                        <li class="breadcrumb-item active"><i class="@foreach($clapps as $p) {{ $p->cla_icon }} @endforeach" style="color: #8898aa!important;"></i> @foreach($clapps as $p) {{ $p->cla_name }} @endforeach</li>
                        <li class="breadcrumb-item active" aria-current="page"> @foreach($clmodule as $p) {{ $p->clm_name }} @endforeach</li>
                        <li id="breadAdditional" class="breadcrumb-item active d-none" aria-current="page"></li>
                        <li id="breadAdditionalText" class="breadcrumb-item active d-none" aria-current="page"></li>
                    </ol>
                </nav>
            </div>
        </div>
        <hr class="mt-0 bg-white mb-2">
    </div>
    <div class="card shadow" id="main-group">
        <div class="card card-header">
            <form class="form-inline">
                <label class="form-control-label pr-5 mb-2">Activity Name</label>
                <input class="form-control mb-2" placeholder="FolderName" id="foldername"  onchange="getFolderName()">
                <button class="form-control-btn btn btn-default mb-2" type="button" data-toggle="modal" data-target="#exampleModal" onclick="refreshTableList()"><i class="fa fa-search"></i></button>
                <button class="form-control-btn btn btn-primary mb-2" type="button" id="btn-current1">Search</button>
            </form>
        </div>
        <div class="card card-body" style="min-height: 365px">
            <div class="d-none" id="alert-success-registrasi">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                    <span class="alert-inner--text"><strong id="regisgroup"></strong>, has registered.</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="d-none" id="alert-success-update">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                    <span class="alert-inner--text"><strong id="update_sales_notification"></strong>, has updated.</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="container-fluid py-2 card d-border-radius-0 mb-2">
                            {{--<div class="form-inline">

                            </div>--}}
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered nowrap" width="100%" id="table-reggroup">
                                    <thead class="bg-gradient-primary text-lighter">
                                    <tr>
                                        {{--<th>ID</th>--}}
                                        <th data-priority="1">filename</th>
                                        <th data-priority="100000">folder</th>
                                        <th data-priority="0">Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content shadow">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Folder List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table-grouplist">
                            <thead class="bg-gradient-primary text-lighter">
                            <tr>
                                <th data-priority="2">Folder Name</th>
                                <th data-priority="1">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                {{--<div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>--}}
            </div>
        </div>
    </div>
    <style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
        }
        
        tr:nth-child(even) {
          background-color: #dddddd;
        }
        </style>
    <div class="card shadow">
        <table>
            <tr>
                <th>Filename</th>
                <th>Folder</th>
                <th>Fullpath</th>
            </tr>            
            @foreach($directories as $p)
            <tr>
                <td>{{ $p->filename }}</td>
                <td>{{ $p->folder }}</td>
                <td><a href={{ route('download',$p->sls)}} class='btn btn-ghost-info'>{{ $p->sls }}</a></td>
            </tr>
            @endforeach 
        </table>
    </div>
@endsection
