@extends('layouts.app-argon')

@section('css')
    <link rel="stylesheet" href="{{ url('bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ url('bootstrap-daterangepicker/bootstrap-daterangepicker.css') }}">
@endsection

@section('js')
    <script src="{{ url('moment/moment.js') }}"></script>
    <script src="{{ url('bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('bootstrap-daterangepicker/bootstrap-daterangepicker.js') }}"></script>
  
    <script src="{{ url('forms_pickers.js') }}"></script>
    <script src="{{ url('inputmask/dist/jquery.inputmask.js') }}"></script>

    <script type="text/javascript">
        var groupname = '';
        var groupphone = '';
        var groupemail = '';

        var rulesobj = {
            "groupname" : {
                required : true
            },
            "groupphone" : {
                required : true,
                digits : true
            },
            "groupemail" : {
                required : true,
                email :true
            },

        };

        var messagesobj = {
            "groupname" : {
                required : "Field is required."
            },
            "groupphone" : {
                required : "Field is required.",
                digits : "Field must be a number."
            },
            "groupemail" : {
                required : "Field is required.",
                email : "Field must be a valid email address."
            },

        };

        $(function () {
            var $form = $('#myFormActivity');
            $form.validate({
                rules: rulesobj,
                messages: messagesobj,
                debug: false,
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback offset-label-error-sales');
                    element.closest('.form-group').append(error);
                    $(element).addClass('is-invalid');
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });

            $form.find("#saveGroup").on('click', function () {
                if ($form.valid()) {
                    var groupid = $("#groupid").val();
                    $.ajax({
                        type: "GET",
                        url: "{{ url('getActivityId') }}",
                        data: {
                            'id': groupid,
                        },
                        success: function (res) {
                            if (res.status === "01") {
                                $("#cekGroupId").text('Sales Id not available, try another ID');
                                $("#groupid").addClass("is-invalid");
                                $("#groupid").focus();
                            } else {
                                savegroup();
                            }
                        }
                    });
                } else {
                    $('.lbl-group').removeClass('focused');
                }
                return false;
            });

            $form.find("#updateActivity").on('click', function () {
                updateActivity($form);
                return false;
            });

            $form.keypress(function(e) {
                if(e.which == 13) {
                    if ($("#hiddenID").val() === ''){
                        $("#saveGroup").click();
                        console.log('save');
                    } else {
                        updateActivity($form);
                        console.log('edit');
                    }
                }
            });
        });

        function updateActivity(form){
            if (form.valid()) {
                swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonClass: "btn-danger",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnCancel: true,
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            updateactivity();
                        }
                    }
                )
            }  else {
                $('.lbl-group').removeClass('focused');
            }
        }

        $(document).ready(function () {
            getTableGroup();
            getTableList();
            $('.js-example-basic-single').select2({
                placeholder: 'AOID'
            });
            $('.bootstrap-select').selectpicker();
        });

        function getDateBips(tanggal){
            var datetime = tanggal.split(" ");
            var tgl = datetime[0].split("-");

            var year = tgl[0];

            if (tgl[1] == '01' ||tgl[1] == '1'){
                var month = 'January';
            } else if (tgl[1] == '02' ||tgl[1] == '2'){
                var month = 'February';
            } else if (tgl[1] == '03' ||tgl[1] == '3'){
                var month = 'March';
            } else if (tgl[1] == '04' ||tgl[1] == '4'){
                var month = 'April';
            } else if (tgl[1] == '05' ||tgl[1] == '5'){
                var month = 'Mei';
            } else if (tgl[1] == '06' ||tgl[1] == '6'){
                var month = 'June';
            } else if (tgl[1] == '07' ||tgl[1] == '7'){
                var month = 'July';
            } else if (tgl[1] == '08' ||tgl[1] == '8'){
                var month = 'August';
            } else if (tgl[1] == '09' ||tgl[1] == '9'){
                var month = 'September';
            } else if (tgl[1] == '10'){
                var month = 'October';
            } else if (tgl[1] == '11'){
                var month = 'November';
            } else if (tgl[1] == '12'){
                var month = 'December';
            }

            var date = tgl[2];

            return date+" "+month+" "+year;
        }

        function refreshTableList(){
            $('#table-grouplist').DataTable().ajax.reload();
        }

        function getTableList() {
            $("#table-grouplist").DataTable({
                /*processing: true,
                serverSide: true,*/
                responsive: true,
                ajax : {
                    url: '{{ url("getDataActivity") }}',
                    data: function (d) {
                        var search_data = {
                            activityID:'',
                        };
                        d.search_param = search_data;
                    },
                },
                columns : [
                    {data : 'name', name: 'name'},
                    {data : 'sls', name: 'sls'},
                    {data : 'sls', name: 'sls'},
                ],
                columnDefs: [{
                    targets: [0],
                    orderable: true,
                    searchable: true,
                },{
                    targets: [1],
                    orderable: true,
                    searchable: true,
                },{
                    searchable : true,
                    targets : [2],
                    className: "text-center",
                    render : function (data, type, row) {
                        return '<i class="text-center">' +
                            '<button class="btn btn-sm btn-primary" type="button" data-dismiss= "modal" onclick="clickOK(\''+data+'\')">Pick</button></i>'}
                },
                ]
            });
        }

        function clickOK(id) {
            $("#activityID").val(id);
            // alert($("#salesID").val());
            getGroup();
        }

        function getTableGroup(){
            var tableGroup = $("#table-reggroup").DataTable({
                responsive: {
                    details: true,
                },
                /*processing: true,
                serverSide: true,*/
                aaSorting: [[0, 'asc']],
                dom: 'l<"toolbar"><f "col-md-6">rtip',
                initComplete: function(){
                    $("div.toolbar").html('<button class="form-control-btn-0 btn btn-primary mb-2" id="addgroup" type="button" onclick="getMaxId()">Add</button>');
                },
                ajax : {
                    url: '{{ url("getDataActivity") }}',
                    data: function (d) {
                        var search_data = {
                            activityID: $("#activityID").val(),
                        };
                        d.search_param = search_data;
                    },
                },
                columns : [
                    {data : 'id', name: 'id'},
                    {data : 'name', name: 'name'},
                    {data : 'counterpart', name: 'counterpart'},
                    {data : 'pic_counterpart', name: 'pic_counterpart'},
                    {data : 'typeactivity', name: 'typeactivity'},
                    {data : 'activityperiod', name: 'activityperiod'},
                    {data : 'date_start', name: 'date_start'},
                    {data : 'date_finish', name: 'date_finish'},
                    {data : 'due_date', name: 'due_date'},
                    {data : 'day_reminder', name: 'day_reminder'},
                    {data : 'data_owner', name: 'data_owner'},
                    {data : 'media', name: 'media'},
                    {data : 'media_content', name: 'media_content'},
                    {data : 'status', name: 'status'},
                    {data : 'regulasi', name: 'regulasi'},
                    {data : 'remarks', name: 'remarks'},
                    {data : 'sls', name: 'sls'},
                ],
                columnDefs: [{
                    targets : [0],
                    orderable : true,
                    searchable : true,
                },{
                    targets : [1],
                    orderable : true,
                    searchable : true,
                    render : function(data, type, row){                    
                        return '<span style="white-space:normal">' + data + "</span>";
                    },                    
                },{
                    targets : [2],
                    orderable : true,
                    searchable : true,
                },{
                    targets : [3],
                    orderable : true,
                    searchable : true,
                },{
                    targets : [4],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [5],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [6],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [7],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [8],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [9],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [10],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [11],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [12],
                    orderable :true,
                    searchable : true,
                    render : function(data, type, row){                    
                        return '<a target="_blank" rel="noopener noreferrer" href="'+data+'">'+data+'</a>';
                    },
                },{
                    targets : [13],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [14],
                    orderable :true,
                    searchable : true,
                },{
                    targets : [15],
                    orderable :true,
                    searchable : true,
                },{
                    searchable : true,
                    targets : [16],
                    className: 'text-center',
                    render : function (data, type, row) {
                        return  '<i class="text-center"><button class="btn btn-sm btn-warning fa fa-pen" onclick="editActivity(\''+data+'\')" type="button" data-dismiss= "modal")"></button>'
                    },
                    className: "text-center",
                },
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: 1 },
                    { responsivePriority: 3, targets: 2 },
                ],
            });
        }

        function savegroup() {

            var groupid = $("#groupid").val()
            var groupname = $("#groupname").val();            
            var groupcounterpart = $("#groupcounterpart").val();            
            var grouppic_counterpart = $("#grouppic_counterpart").val();
            var grouptypeactivity = $("#grouptypeactivity").val();
            var groupactivityperiod = $("#groupactivityperiod").val();
            var groupdate_start = $("#groupdate_start").val();
            var groupdate_finish = $("#groupdate_finish").val();
            var groupdue_date = $("#groupdue_date").val();
            var groupday_reminder = $("#groupday_reminder").val();
            var groupdata_owner = $("#groupdata_owner").val();
            var groupmedia = $("#groupmedia").val();
            var groupmedia_content = $("#groupmedia_content").val();
            var groupstatus = $("#groupstatus").val();
            var groupregulasi = $("#groupregulasi").val();
            var groupremarks = $("#groupremarks").val();            
            var required = "Field is required.";
            $.get("/mockjax");

            $.ajax({
                type: "GET",
                url: "{{ url('activity-registrasi') }}",
                data: {
                    'id': groupid,
                    'name': groupname,
                    'counterpart': groupcounterpart,
                    'pic_counterpart': grouppic_counterpart,
                    'typeactivity': grouptypeactivity,
                    'activityperiod': groupactivityperiod,
                    'date_start': groupdate_start,
                    'date_finish': groupdate_finish,
                    'due_date': groupdue_date,
                    'day_reminder': groupday_reminder,
                    'data_owner': groupdata_owner,
                    'media': groupmedia,
                    'media_content': groupmedia_content,
                    'status': groupstatus,
                    'regulasi': groupregulasi,
                    'remarks': groupremarks,
                },
                success: function (res) {
                    if ($.trim(res)) {
                        if (res.status === "00") {
                            $('#table-reggroup').DataTable().ajax.reload();
                            $("#add-group").removeClass("d-block");
                            $("#add-group").addClass("d-none");
                            $("#main-group").removeClass("d-none");
                            $("#main-group").addClass("d-block");
                            $("#regisgroup").text(res.group);
                            $("#alert-success-registrasi").removeClass("d-none");
                            $("#alert-success-registrasi").addClass("d-block");

                            clearVariable();
                        } else {
                            $("#err_msg").text(res.err_msg);
                            $("#alert-error-registrasi").addClass("d-block");
                            $("#alert-error-registrasi").removeClass("d-none");
                        }
                    }
                }
            });
        }

        function updateactivity() {

            var groupid = $("#hiddenID").val();
            var groupname = $("#groupname").val();
            var groupcounterpart = $("#groupcounterpart").val();
            var grouppic_counterpart = $("#grouppic_counterpart").val();
            var grouptypeactivity = $("#grouptypeactivity").val();
            var groupactivityperiod = $("#groupactivityperiod").val();
            var groupdate_start = $("#groupdate_start").val();
            var groupdate_finish = $("#groupdate_finish").val();
            var groupdue_date = $("#groupdue_date").val();
            var groupday_reminder = $("#groupday_reminder").val();
            var groupdata_owner = $("#groupdata_owner").val();
            var groupmedia = $("#groupmedia").val();
            var groupmedia_content = $("#groupmedia_content").val();
            var groupstatus = $("#groupstatus").val();
            var groupregulasi = $("#groupregulasi").val();
            var groupremarks = $("#groupremarks").val();            

            $.get("/mockjax");

            $.ajax({
                type: "GET",
                url: "{{ url('activity-update/submit') }}",
                data: {
                    'id': groupid,
                    'name': groupname,
                    'counterpart': groupcounterpart,
                    'pic_counterpart': grouppic_counterpart,
                    'typeactivity': grouptypeactivity,
                    'activityperiod': groupactivityperiod,
                    'date_start': groupdate_start,
                    'date_finish': groupdate_finish,
                    'due_date': groupdue_date,
                    'day_reminder': groupday_reminder,
                    'data_owner': groupdata_owner,
                    'media': groupmedia,
                    'media_content': groupmedia_content,
                    'status': groupstatus,
                    'regulasi': groupregulasi,
                    'remarks': groupremarks,
                },
                success: function (res) {
                    if ($.trim(res)) {
                        if (res.status === "00") {
                            $('#table-reggroup').DataTable().ajax.reload();
                            $("#add-group").removeClass("d-block");
                            $("#add-group").addClass("d-none");
                            $("#main-group").removeClass("d-none");
                            $("#main-group").addClass("d-block");
                            $("#update_sales_notification").text(res.group);
                            $("#alert-success-update").removeClass("d-none");
                            $("#alert-success-update").addClass("d-block");

                            clearVariable();
                        } else {
                            $("#err_msg").text(res.err_msg);
                            $("#alert-error-registrasi").addClass("d-block");
                            $("#alert-error-registrasi").removeClass("d-none");
                        }
                    }
                }
            });
        }

        function getMaxId() {
            
            $("#groupid").removeAttr( "disabled", "disabled" );
            var id = $("#addgroupID").val();            
            $.ajax({
                type : "GET",
                url  : "{{ url('get-maxid') }}",
                success : function (res) {
                    if ($.trim(res)){
                        $("#addgroupID").val(res);
                        $id = Number(res) + 1;
                        console.log($id);
                        $("#groupid").val($id);
                        $("#groupid").attr( "disabled", "disabled" );
                        
                    }
                }
            });

            $("#hiddenID").val('');
            $("#groupname").val('');

            $("#add-group").removeClass("d-none");
            $("#add-group").addClass("d-block");
            $("#main-group").removeClass("d-block");
            $("#main-group").addClass("d-none");
            $("#breadAdditional").removeClass("d-none").addClass("d-block").text("Add");


            $("#savegroupbutton").addClass('d-block');
            $("#savegroupbutton").removeClass('d-none');
            $("#editgroupbutton").removeClass('d-block');
            $("#editgroupbutton").addClass('d-none');

            clearCache();
        }

        function changeFormat(olddate){
            //2023-01-01
            $year = olddate.substring(0, 4);
            $month = olddate.substring(5, 7);
            $day = olddate.substring(8, 10);
            console.log($year+" "+$month+" "+$day);
            return $month+"/"+$day+"/"+$year;
        }

        function editActivity(data){
            var urldownload = "{{ url('downloadbyid') }}";
            $("#groupid").attr( "disabled", "disabled" );
            $.ajax({
                type : "GET",
                        url  : "{{ url('activity-update/') }}",
                        data : {
                        'activityID' : data,
                    },
                    success : function (res) {
                        // console.log(res.name);
                        $("#breadAdditional").removeClass("d-none").addClass("d-block").text("Edit");
                        $("#breadAdditionalText").removeClass("d-none").addClass("d-block").text(res.name);
                        $("#groupid").val(data);
                        $("#groupname").val(res.name);
                        $("#groupcounterpart").val(res.counterpart);
                        $("#grouppic_counterpart").val(res.pic_counterpart);
                        $("#grouptypeactivity").val(res.typeactivity);
                        $("#groupactivityperiod").val(res.activityperiod);  
                        $date_start = changeFormat(res.date_start);
                        $("#groupdate_start").val($date_start);
                        //$("#groupdate_start").datepicker('setDate', '01/03/2023');
                        $("#groupdate_start").datepicker('setDate', $date_start);
                        $date_finish = changeFormat(res.date_finish);
                        $("#groupdate_finish").val($date_finish);
                        $("#groupdate_finish").datepicker('setDate', $date_finish);
                        $due_date = changeFormat(res.due_date);
                        $("#groupdue_date").val($due_date);
                        $("#groupdue_date").datepicker('setDate', $due_date);                        
                        $day_reminder = changeFormat(res.day_reminder);
                        $("#groupday_reminder").val($day_reminder);
                        $("#groupday_reminder").datepicker('setDate', $day_reminder);
                        $("#groupdata_owner").val(res.data_owner);
                        $("#groupmedia").val(res.media);
                        $("#groupmedia_content").val(res.media_content);
                        $("#groupdata_owner").val(res.data_owner);
                        $("#groupstatus").val(res.status);
                        $("#groupregulasi").val(res.regulasi);
                        $("#groupremarks").val(res.remarks);

                        groupid = res.id;
                        groupname = res.name;
                        groupcounterpart = res.counterpart;
                        grouppic_counterpart = res.pic_counterpart;
                        grouptypeactivity = res.typeactivity;
                        groupactivityperiod = res.activityperiod;
                        groupdate_start = res.groupdate_start;
                        groupdate_finish = res.groupdate_finish;
                        groupdue_date = res.due_date;
                        groupday_reminder = res.day_reminder;
                        groupdata_owner = res.data_owner;
                        groupmedia = res.media;
                        groupmedia_content = res.media_content;
                        groupstatus = res.status;
                        groupregulasi= res.regulasi;
                        groupremarks= res.remarks;
                }
            });
            $.ajax({
                type : "GET",
                url  : "{{ url('activity-getfiles/') }}",
                data : {
                        'activityID' : data,
                    },
                dataType: "json",
                success : function (dataResult) {

                    console.log(dataResult[0]);
                    var resultData = dataResult.data;
                    var txt = '';
                    var i=0;
                    $.each(dataResult,function(index,row){
                        console.log(index+" "+row.id);
                        var downloadUrl = urldownload+'/'+row.id;
                        var editUrl = urldownload+'/'+row.id;
                        console.log("downloadUrl: "+downloadUrl);
                        txt+="<tr>"
                            txt+="<td>"+row.id+"</td><td><a href='"+downloadUrl+"' class='btn btn-ghost-info'>"+row.file+"</a></td>"
                        +"<td><a class='btn btn-primary' href='"+editUrl+"'>Download</a>" 
                        +"<button class='btn btn-danger delete' value='"+row.id+"' style='margin-left:20px;'>Delete</button></td>";
                        txt+="</tr>";
                        
                    })
                    //console.log(txt);
                    $("#bodyData").replaceWith(txt);                    
                }
            });

            // $("#groupname").val('');
            $("#hiddenID").val(data);

            $("#add-group").removeClass("d-none");
            $("#add-group").addClass("d-block");
            $("#main-group").removeClass("d-block");
            $("#main-group").addClass("d-none");
            $("#savegroupbutton").addClass('d-none');
            $("#savegroupbutton").removeClass('d-block');
            $("#editgroupbutton").removeClass('d-none');
            $("#editgroupbutton").addClass('d-block');
            clearCache();
        }

        $("#resetgroup").on('click', function(){
            cacheError();
            var data = $("#hiddenID").val()
            $.ajax({
                type : "GET",
                url  : "{{ url('activity-update/') }}",
                data : {
                    'activityID' : data,
                },
                success : function (res) {
                    console.log(res.name);
                    $("#breadAdditional").removeClass("d-none").addClass("d-block").text("Edit");
                    $("#breadAdditionalText").removeClass("d-none").addClass("d-block").text(res.name);
                    $("#groupid").val(data);
                    $("#groupname").val(res.name);
                    $("#groupcounterpart").val(res.counterpart);
                    $("#grouppic_counterpart").val(res.pic_counterpart);
                    $("#grouptypeactivity").val(res.typeactivity);
                    $("#groupactivityperiod").val(res.activityperiod);  
                    $date_start = changeFormat(res.date_start);
                    $("#groupdate_start").val($date_start);
                    //$("#groupdate_start").datepicker('setDate', '01/03/2023');
                    $("#groupdate_start").datepicker('setDate', $date_start);
                    $date_finish = changeFormat(res.date_finish);
                    $("#groupdate_finish").val($date_finish);
                    $("#groupdate_finish").datepicker('setDate', $date_finish);
                    $due_date = changeFormat(res.due_date);
                    $("#groupdue_date").val($due_date);
                    $("#groupdue_date").datepicker('setDate', $due_date);
                    $day_reminder = changeFormat(res.day_reminder);
                    $("#groupday_reminder").val($day_reminder);
                    $("#groupday_reminder").datepicker('setDate', $day_reminder);
                    $("#groupdata_owner").val(res.data_owner);
                    $("#groupmedia").val(res.media);
                    $("#groupmedia_content").val(res.media_content);
                    $("#groupdata_owner").val(res.data_owner);
                    $("#groupstatus").val(res.status);
                    $("#groupregulasi").val(res.regulasi);
                    $("#groupremarks").val(res.remarks);
                }
            });
        });

        function cacheError() {
            $('.lbl-group').removeClass('focused');            

            $("#groupid-error").text('');
            $("#groupname-error").text('');
            $("#groupphone-error").text('');
            $("#groupemail-error").text('');
            $("#groupstatus-error").text('');

            $("#groupid").removeClass("is-invalid");
            $("#groupname").removeClass("is-invalid");
            $("#groupphone").removeClass("is-invalid");
            $("#groupemail").removeClass("is-invalid");
            $("#groupstatus").removeClass("is-invalid");

            $("#cekGroupId").text('');
            $("#cekGroupname").text('');
            $("#cekGroupPhone").text('');
            $("#cekGroupEmail").text('');
            $("#cekGroupStatus").text('');
        }

        function clearCache(){
            cacheError();

            $("#hiddendealerid").val('');
            //$("#groupid").val('');
            $("#groupname").val('');
            $("#groupcounterpart").val('');
            $("#grouppic_counterpart").val('');
            $("#grouptypeactivity").val('');
            $("#groupactivityperiod").val('');
            $("#groupdate_start").val('');
            $("#groupdate_finish").val('');
            $("#groupdue_date").val('');
            $("#groupday_reminder").val('');
            $("#groupdata_owner").val('');
            $("#groupmedia").val('');
            $("#groupmedia_content").val('');
            $("#groupdata_owner").val('');
            $("#groupstatus").val('');
            $("#groupregulasi").val('');
            $("#groupremarks").val('');

            $("#alert-error-registrasi").removeClass("d-block");
            $("#alert-error-registrasi").addClass("d-none");

            $("#alert-success-registrasi").removeClass("d-block");
            $("#alert-success-registrasi").addClass("d-none");

            $("#alert-success-update").removeClass("d-block");
            $("#alert-success-update").addClass("d-none");
        }

        $("#cancelgroup").on("click", function () {
            var res =  $("#hiddenID").val()+$("#groupid").val()+$("#groupname").val()+$("#groupaddress").val()+$("#groupphone").val()+$("#groupemail").val()+$("#groupstatus").val();
            res = res.trim();
            if(res.length > 0){
                swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonClass: "btn-danger",
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnCancel: true,
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $("#add-group").removeClass("d-block");
                            $("#add-group").addClass("d-none");
                            $("#main-group").removeClass("d-none");
                            $("#main-group").addClass("d-block");
                            $("#breadAdditional").removeClass("d-block").addClass("d-none").text("");
                        }
                    }
                )

            }else{
                $("#add-group").removeClass("d-block");
                $("#add-group").addClass("d-none");
                $("#main-group").removeClass("d-none");
                $("#main-group").addClass("d-block");
                $("#breadAdditional").removeClass("d-block").addClass("d-none").text("");
            }
        });

        function clearVariable() {

            groupid = '';
            groupname = '';
            groupcounterpart = '';
            grouppic_counterpart = '';
            grouptypeactivity = '';
            groupactivityperiod = '';
            groupdate_start = '';
            groupdate_finish = '';
            groupdue_date = '';
            groupday_reminder = '';
            groupuploadfile = "";
        }

        function cancelEdit(){
            $("#breadAdditional").removeClass("d-block").addClass("d-none").text('');
            $("#breadAdditionalText").removeClass("d-block").addClass("d-none").text('');
            $("#add-group").removeClass("d-block");
            $("#add-group").addClass("d-none");
            $("#main-group").removeClass("d-none");
            $("#main-group").addClass("d-block");

            clearVariable();
        }

        $("#canceleditgroup").on("click", function () {
            var groupnameN = $("#groupname").val();

            var groupphoneN = $("#groupphone").val();
            var groupemailN = $("#groupemail").val();


            if (groupname === groupnameN && groupphone === groupphoneN &&
                     groupemail === groupemailN) {
                cancelEdit();
            } else {
                swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonClass: "btn-danger",
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnCancel: true,
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            cancelEdit();
                        }
                    }
                )
            }
        });

        $("#btn-current1").on("click", function(){

            getGroup();
        });

        function getGroup() {
            var id = $("#activityID").val();
            console.log(id);
            if(id === ''){
                $("#groupGet").val('');
                $('#table-reggroup').DataTable().ajax.reload();
            } else {
                $.ajax({
                    type : "GET",
                    url  : "{{ url('activityGetName') }}",
                    data : {
                        'activityID' : id,
                    },
                    success : function (res) {
                        if ($.trim(res)){
                            $("#groupGet").val(res[0].id);
                        } else {
                            $("#groupGet").val('');
                        }
                        $('#table-reggroup').DataTable().ajax.reload();
                    }
                });
            }
        }
        
        function handleChangeDate(){        
        }           

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
        $("#importExcel").on('show.bs.modal', function(){
            
            //alert('The modal is about to be shown.');
            $groupid = $("#groupid").val();
            console.log($groupid);
            $("#modalgroupid").val($groupid);
            //$("#modalgroupid").attr( "disabled", "disabled" );
            
        });        
    </script>
@endsection

@section('content')
    <div class="modal-ajax"></div>
    <div class="header text-white">
        <div class="row col-xs-0">
            <div class="col-sm-12 col-xs-12">
                <nav aria-label="breadcrumb" class="d-inline-block ml-0 w-100">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark mb-2">
                        {{--<li class="breadcrumb-item"><a href="#"><i class="ni ni-single-02"></i> Dashboards</a></li>--}}
                        <li class="breadcrumb-item active"><i class="@foreach($clapps as $p) {{ $p->cla_icon }} @endforeach" style="color: #8898aa!important;"></i> @foreach($clapps as $p) {{ $p->cla_name }} @endforeach</li>
                        <li class="breadcrumb-item active" aria-current="page"> @foreach($clmodule as $p) {{ $p->clm_name }} @endforeach</li>
                        <li id="breadAdditional" class="breadcrumb-item active d-none" aria-current="page"></li>
                        <li id="breadAdditionalText" class="breadcrumb-item active d-none" aria-current="page"></li>
                    </ol>
                </nav>
            </div>
        </div>
        <hr class="mt-0 bg-white mb-2">
    </div>

    <div class="card shadow" id="main-group">
        <div class="card card-header">
            <form class="form-inline">
                <label class="form-control-label pr-5 mb-2">Activity Name</label>
                <input class="form-control mb-2" placeholder="Input Activity ID" id="activityID"  onchange="getGroup()">
                <input class="form-control mb-2 ml-input-2" placeholder="Activity Name" readonly id="groupGet">
                <button class="form-control-btn btn btn-default mb-2" type="button" data-toggle="modal" data-target="#exampleModal" onclick="refreshTableList()"><i class="fa fa-search"></i></button>
                <button class="form-control-btn btn btn-primary mb-2" type="button" id="btn-current1">Search</button>
            </form>
        </div>
        <div class="card card-body" style="min-height: 365px">
            <div class="d-none" id="alert-success-registrasi">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                    <span class="alert-inner--text"><strong id="regisgroup"></strong>, has registered.</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="d-none" id="alert-success-update">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                    <span class="alert-inner--text"><strong id="update_sales_notification"></strong>, has updated.</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="container-fluid py-2 card d-border-radius-0 mb-2">
                            {{--<div class="form-inline">

                            </div>--}}
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered nowrap" width="100%" id="table-reggroup">
                                    <thead class="bg-gradient-primary text-lighter">
                                    <tr>
                                        {{--<th>ID</th>--}}
                                        <th data-priority="0">id</th>
                                        <th data-priority="2">Name</th>
                                        <th data-priority="10004">Counterpart</th>
                                        <th data-priority="10002">pic_counterpart</th>
                                        <th data-priority="10003">typeactivity</th>
                                        <th data-priority="10005">activityperiod</th>
                                        <th data-priority="5">date_start</th>
                                        <th data-priority="6">date_finish</th>
                                        <th data-priority="3">due_date</th>
                                        <th data-priority="4">day_reminder</th>
                                        <th data-priority="7">data_owner</th>
                                        <th data-priority="8">media</th>
                                        <th data-priority="9">media_content</th>
                                        <th data-priority="10">status</th>
                                        <th data-priority="11">regulasi</th>
                                        <th data-priority="12">remarks</th>
                                        <th data-priority="1">Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="card shadow d-none" id="add-group">
        <form id="myFormActivity" method="post" action="{{ url('/uploadFile') }}" enctype="multipart/form-data">
            <input type="hidden" id="hiddenID">
            <div class="card card-body" style="min-height: 365px">

                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-body">
                            <div class="d-none" id="alert-error-registrasi">
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <span class="alert-inner--icon"><i class="fa fa-exclamation-triangle"></i></span>
                                    <span class="alert-inner--text"><strong>Err.</strong><span id="err_msg"></span></span>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="container-fluid py-2 card d-border-radius-0 mb-2">
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Activity ID</label>
                                    <input class="form-control col-sm-6" type="text" placeholder="Activity ID" id="groupid" name="groupid" onchange="cacheError();"/>
                                    <label id="cekGroupID" class="error invalid-feedback small d-block col-sm-4" for="groupid"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Name</label>
                                    <input class="form-control col-sm-6" type="text" placeholder="Activity Name" id="groupname" name="groupname" onchange="cacheError();"/>
                                    <label id="cekGroupname" class="error invalid-feedback small d-block col-sm-4" for="groupname"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Counterpart</label>
                                    <select class="form-control col-sm-6" id="groupcounterpart" name="groupcounterpart" onchange="cacheError();">
                                        @foreach($counterpartlist as $p)
                                        <option value="{{ $p->name }}">{{ $p->name}}</option>
                                        @endforeach 
                                    </select>
                                    <label id="cekGroupCounterpart" class="error invalid-feedback small d-block col-sm-4" for="groupcounterpart"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">pic counterpart</label>
                                    <select class="form-control col-sm-6" id="grouppic_counterpart" name="grouppic_counterpart" onchange="cacheError();">
                                        @foreach($user_admins as $p)
                                        <option value="{{ $p->username }}">{{ $p->username}}</option>
                                        @endforeach 
                                    </select>
                                    <label id="cekGroupPicCounterpart" class="error invalid-feedback small d-block col-sm-4" for="grouppic_counterpart"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Type Activity ID</label>
                                    <select class="form-control col-sm-6" id="grouptypeactivity" name="grouptypeactivity" onchange="cacheError();">
                                        @foreach($typeactivity as $p)
                                        <option value="{{ $p->name }}">{{ $p->name}}</option>
                                        @endforeach 
                                    </select>
                                    <label id="cekGroupTypeActivityID" class="error invalid-feedback small d-block col-sm-4" for="grouptypeactivity"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Activity Period ID</label>
                                    <select class="form-control col-sm-6" id="groupactivityperiod" name="groupactivityperiod" onchange="cacheError();">
                                        @foreach($activityperiod as $p)
                                        <option value="{{ $p->name }}">{{ $p->name}}</option>
                                        @endforeach 
                                    </select>
                                    <label id="cekGroupActivityPeriodID" class="error invalid-feedback small d-block col-sm-4" for="groupactivityperiod"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Date Start</label>
                                    <input class="form-control datepicker col-sm-6" id="groupdate_start" placeholder="Date Start" type="text" onchange="handleChangeDate()" value="{{ date('m/d/Y') }} ">
                                    <label id="cekGroupDateStart" class="error invalid-feedback small d-block col-sm-4" for="groupdate_start"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Date Finish</label>
                                    <input class="form-control datepicker col-sm-6" id="groupdate_finish" placeholder="Date Finish" type="text" onchange="handleChangeDate()" value="{{ date('m/d/Y') }} ">
                                    <label id="cekGroupDateFinish" class="error invalid-feedback small d-block col-sm-4" for="groupdate_finish"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Due Date</label>
                                    <input class="form-control datepicker col-sm-6" id="groupdue_date" placeholder="Due Date" type="text" onchange="handleChangeDate()" value="{{ date('m/d/Y') }} ">
                                    <label id="cekGroupDueDate" class="error invalid-feedback small d-block col-sm-4" for="groupdue_date"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Day Reminder</label>
                                    <input class="form-control datepicker col-sm-6" id="groupday_reminder" placeholder="Day Reminder" type="text" onchange="handleChangeDate()" value="{{ date('m/d/Y') }} ">
                                    <label id="cekGroupDayReminder" class="error invalid-feedback small d-block col-sm-4" for="groupday_reminder"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Data Owner</label>
                                    <select class="form-control col-sm-6" id="groupdata_owner" name="groupdata_owner" onchange="cacheError();">
                                        @foreach($data_owner as $p)
                                        <option value="{{ $p->name }}">{{ $p->name}}</option>
                                        @endforeach 
                                    </select>
                                    <label id="cekGroupdata_owner" class="error invalid-feedback small d-block col-sm-4" for="groupdata_owner"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Media</label>
                                    <select class="form-control col-sm-6" id="groupmedia" name="groupmedia" onchange="cacheError();">
                                        @foreach($media as $p)
                                        <option value="{{ $p->name }}">{{ $p->name}}</option>
                                        @endforeach 
                                    </select>
                                    <label id="cekGroupmedia" class="error invalid-feedback small d-block col-sm-4" for="groupmedia"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Name</label>
                                    <input class="form-control col-sm-6" type="text" placeholder="Media Content" id="groupmedia_content" name="groupmedia_content" onchange="cacheError();"/>
                                    <label id="cekGroupmedia_content" class="error invalid-feedback small d-block col-sm-4" for="groupmedia_content"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Status</label>
                                    <select class="form-control col-sm-6" id="groupstatus" name="groupstatus" onchange="cacheError();">
                                        @foreach($status as $p)
                                        <option value="{{ $p->name }}">{{ $p->name}}</option>
                                        @endforeach 
                                    </select>
                                    <label id="cekGroupstatus" class="error invalid-feedback small d-block col-sm-4" for="groupstatus"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Regulasi</label>
                                    <input class="form-control col-sm-6" type="text" placeholder="Regulasi" id="groupregulasi" name="groupregulasi" onchange="cacheError();"/>
                                    <label id="cekGroupregulasi" class="error invalid-feedback small d-block col-sm-4" for="groupregulasi"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Remarks</label>
                                    <input class="form-control col-sm-6" type="text" placeholder="Remarks" id="groupremarks" name="groupremarks" onchange="cacheError();"/>
                                    <label id="groupremarks" class="error invalid-feedback small d-block col-sm-4" for="groupremarks"></label>
                                </div>
                                <div class="form-group form-inline lbl-group">
                                    <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Files</label>                                    
                                    <table class="table table-bordered table-sm">
                                        <thead>
                                         <tr>
                                             <th>id</th>
                                             <th>file</th>
                                             <th width="280px">Action</th>
                                         </tr>
                                        </thead>
                                        <tbody id="bodyData">
                                 
                                        </tbody>  
                                     </table>                                    
                                     <button type="button" class="form-control-btn-0 btn btn-outline-default mb-2" data-toggle="modal" data-target="#importExcel">
                                        <i style="color: #00b862" class="fa fa-upload"></i> Upload File</button>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="card card-footer text-right">
                <div class="form-inline justify-content-end" id="savegroupbutton">
                    <button class="form-control-btn btn btn-primary mb-2" type="button" id="saveGroup">Save</button>
                    <button class="form-control-btn btn btn-info mb-2" type="reset" onclick="cacheError();">Reset</button>
                    <button class="form-control-btn btn btn-danger mb-2" type="button" id="cancelgroup">Cancel</button>
                </div>
                <div class="form-inline justify-content-end d-none" id="editgroupbutton">
                    <button class="form-control-btn btn btn-success mb-2" type="button" id="updateActivity">Update</button>
                    <button class="form-control-btn btn btn-info mb-2" type="reset" id="resetgroup">Reset</button>
                    <button class="form-control-btn btn btn-danger mb-2" type="button" id="canceleditgroup">Cancel</button>
                </div>
            </div>
        </form>
    </div>
    <!-- Modal Group List -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content shadow">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Activity List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table-grouplist">
                            <thead class="bg-gradient-primary text-lighter">
                            <tr>
                                <th data-priority="2">Name</th>
                                <th data-priority="0">counterpart_id</th>
                                <th data-priority="1">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                {{--<div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>--}}
            </div>
        </div>
    </div>
    <!-- Import Excel -->
    <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="uploadFileModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="{{ url('/uploadFileNew') }}" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="uploadFileModalLabel">Upload file</h5>
                    </div>
                    <div class="modal-body">

                        {{ csrf_field() }}
                        <div>
                            <label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Activity ID</label>
                            <input class="form-control col-sm-12" type="text" placeholder="Activity ID" id="modalgroupid" name="modalgroupid" 
                            onchange="cacheError();" readonly="true" 
                            />
                        </div>
                        <div class="custom-file mb-3">   
                            <!--<label class="form-control-label form-inline-label col-sm-2 mb-2 px-0">Activity ID</label>                         
                            <input type="text" id="modalgroupid" name="modalgroupid" class="form-control col-sm-6" readonly="true" />-->

                            <input type="file" class="custom-file-input" id="customFile" name="file" required="required">
                            <label class="custom-file-label" for="customFile"
                            style="white-space: nowrap;
                            width: 100%;
                            overflow: hidden;
                            text-overflow: ellipsis;"
                              >Choose file</label>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

