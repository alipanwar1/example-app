<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index', 301);

Auth::routes();
Route::group(['middleware' => 'auth'], function() {
	//Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/user', 'UserController@user')->name('useradmin.user');
	Route::get('/sales', 'SalesController@sales')->name('masterdata.sales');
	Route::get('/useradmin', 'PrivilegeController@useradmin')->name('adminprivilege.upadmin');
	Route::get('getDataSales','SalesController@getSales');
	Route::get('sales-registrasi', 'SalesController@registrasiSales');
    Route::get('sales-update', 'SalesController@salesEdit');
    Route::get('sales-update/submit', 'SalesController@updateSales');
    Route::get('getSalesId','SalesController@getIdSales');
    Route::get('salesGetName', 'SalesController@getSalesName');
	Route::get('/dashboard', 'DashboardActivityController@dashboard')->name('dashboard');
    Route::get('countuseractivity-get', 'DashboardActivityController@countUserActivityLogin');
    Route::get('/dashboardfull', 'DashboardController@dashboardfull')->name('dashboardfull');
	Route::get('/compliance', 'ComplianceController@sales')->name('compliance.compliance');
	Route::get('/counterpart', 'CounterpartController@sales')->name('compliance.counterpart');
	Route::get('getDataCounterpart', 'CounterpartController@getCounterpart');
    Route::get('counterpart-registrasi', 'CounterpartController@registrasiSales');
    Route::get('counterpart-update', 'CounterpartController@salesEdit');
    Route::get('counterpart-update/submit', 'CounterpartController@updateSales');
    Route::get('getCounterpartId','CounterpartController@getIdSales');
    Route::get('counterpartGetName', 'CounterpartController@getSalesName');

	Route::get('/activity', 'Activity2Controller@sales')->name('compliance.activity');
	Route::get('getDataActivity', 'Activity2Controller@getActivity');
    Route::get('activity-registrasi', 'Activity2Controller@registrasiActivity');
    Route::get('activity-update', 'Activity2Controller@activityEdit');
    Route::get('activity-getfiles', 'Activity2Controller@getFiles');
    Route::get('activity-update/submit', 'Activity2Controller@updateActivity');
    Route::get('getActivityId','Activity2Controller@getIdSales');
    Route::get('activityGetName', 'Activity2Controller@getSalesName');
	Route::get('get-maxid', 'Activity2Controller@getMaxId');

	Route::get('/useradmin', 'PrivilegeController@useradmin')->name('adminprivilege.upadmin');
    Route::get('/roleadmin', 'PrivilegeController@roleadmin')->name('adminprivilege.uradmin');
    Route::get('/userlog','PrivilegeController@userlog')->name('adminprivilege.uladmin');
    Route::get('get-dataLog/', 'PrivilegeController@dataLog')->name('data-log');
    Route::get('nameadmin-check', 'PrivilegeController@checkNameAdmin');
    Route::get('get-dataRole/{id}', 'PrivilegeController@dataRole')->name('data-role');
    Route::get('roleadmin-update', 'PrivilegeController@roleadminEdit');
    Route::get('rolenameadmin-registrasi', 'PrivilegeController@registrasiRoleAdmin');
    Route::get('roleadmin-update/submit', 'PrivilegeController@updateRoleadmin');
    Route::get('rolenameadmin-get', 'PrivilegeController@getRolenameAdmin');
    Route::get('rolenameadmin-checkclapp', 'PrivilegeController@checkclApp');
    Route::get('roleadmin-privilege/submit', 'PrivilegeController@updateRoleadminPrivilege');
    Route::get('usermanage-privilege/submit', 'PrivilegeController@updateUserManagePrivilege');
    Route::get('rolenameadmin-checkclusermanage', 'PrivilegeController@checkclUserManage');
    Route::get('userActivity-get', 'UserController@dataUserActivity');

    Route::get('useradmin-delete/submit', 'PrivilegeController@deleteUseradmin');
    Route::get('useradmin-pin', 'PrivilegeController@useradminPin');
    Route::get('useradmin-pin/submit', 'PrivilegeController@pinUseradmin');
    Route::get('useradmin-update', 'PrivilegeController@useradminEdit');
    Route::get('useradmin-update/submit', 'PrivilegeController@updateUseradmin');
    Route::get('usernameadmin-registrasi', 'PrivilegeController@registrasiUserAdmin');
    Route::get('usernameadmin-get', 'PrivilegeController@getUsernameAdmin');
    Route::get('usernameadmin-check', 'PrivilegeController@checkUsernameAdmin');
    Route::get('get-dataAdmin/{id}', 'PrivilegeController@dataAdmin')->name('data-admin');

	//Route::get('/filemanager', 'FileManagerController@dashboard')->name('dashboard');

	Route::get('/filemanager', 'FileManagerController@index')->name('index');
	//Route::get('/filemanager', [FileManagerController::class, 'index']);
	Route::post('/uploadFile', 'FileManagerController@uploadFile')->name('uploadFile');
    Route::post('/uploadFileNew', 'FileManagerController@uploadFileNew')->name('uploadFileNew');
    Route::get('getDataFolder', 'FileManagerController@getDataFolder');
    Route::get('folderGetName', 'FileManagerController@getDataFolder');
    Route::get('selectfolder', 'FileManagerController@selectFolder');
    Route::get('download/{id}', 'FileManagerController@download')->name('download');
    Route::get('downloadbyid/{id}', 'FileManagerController@downloadbyid')->name('downloadbyid');
    Route::get('kirim-email','MailController@index');
});

